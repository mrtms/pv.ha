# Puesta en marcha y configuración de HomeAssistant

**Versión actual 2.x**: 2024.8.x

Los ficheros y la documentación incluidos en este repositorio, son un registro de mi configuración actual de _HomeAssistant_, así como de los pasos necesarios para reconstruirla desde 0 en caso de ser necesario. La instalación y documentación de la misma no están finalizadas, por lo que habrá cosas que podrán ir cambiando en el transcurso del tiempo.

Los supuestos principales por los que se rige la instalación son los siguientes:

- Tratar de que la instalación sea autónoma y que no dependa de servicios externos para su funcionamiento.
- Evitar en lo posible la instalación de _add-ons_ no incluidos en la distribución oficial de _HomeAssistant_ que, con el tiempo, es fácil que queden abandonados y sin soporte.
- La configuración estará realizada en la medida de lo posible mediante ficheros de texto usando _YAML_, ya que facilita en gran manera su reproducción, estudio y compartición.
- Reducir todo lo posible la dependencia del interfaz gráfico para obtener información de estados de _HomeAssistant_.

## Contenido

### Instalación

- [Instalación de HomeAssistant](docs/021_instalacion_de_homeassistant.md)

### Configuración de HomeAssistant

- [Configuración básica inicial](docs/031_configuracion_basica_inicial.md)
- [Configuración del acceso con HTTPS](docs/032_configuracion_acceso_https.md)
- [Configuración de registro y consulta de información](docs/033_configuracion_registro_informacion.md)
- [Configuración de notificaciones en Telegram](docs/041_configuracion_notificaciones_telegram.md)
- [Configuración de sensores de monitorización](docs/059_configuracion_sensores_monitorizacion.md)

#### Integración y organización de elementos en HomeAssistant

- [Denominación y organización de elementos](docs/061_denominacion_organizacion_elementos.md)

#### Integración de nuevos elementos

- [Integración de dispositivos Zigbee](docs/071_integracion_zigbee.md)
- [Integración de dispositivos Shelly](docs/072_integracion_shelly.md)
- [Integración de un SAI empleando NUT](docs/073_integracion_nut.md)
- [Integraciones de clima e información meteorológica](docs/074_integraciones_clima.md)
- [Integraciones de consumo energético](docs/075_integraciones_consumo_energetico.md)
- [Configuración de otros sensores de monitorización](docs/089_configuracion_otros_sensores_monitorizacion.md)

#### Personalización de elementos

- [Personalización de entidades](docs/091_personalizacion_entidades.md)

### Configuración de Lovelace en HomeAssistant

- [Configuración inicial de Lovelace](docs/111_configuracion_inicial_lovelace.md)

#### Instalación de componentes desarrollados por terceros

- [Instalación de tarjetas para Lovelace](docs/131_instalacion_tarjetas_lovelace.md)

##### Creación de paneles de información

- [Creación de un panel: PV-DB](docs/151_creacion_panel_pv-db.md)
  - [Vista: Home](docs/151_creacion_panel_pv-db.md#vista-home)
  - [Vista: Host](docs/151_creacion_panel_pv-db.md#vista-host)
  - [Vista: Network](docs/151_creacion_panel_pv-db.md#vista-network)
  - [Vista: Sensor](docs/151_creacion_panel_pv-db.md#vista-sensor)
  - [Vista: Energy](docs/151_creacion_panel_pv-db.md#vista-energy)
  - [Vista: Automation](docs/151_creacion_panel_pv-db.md#vista-automation)
- [Creación de un panel: PVState-DB](docs/152_creacion_panel_pvstate-db.md)
  - [Vista: State](docs/152_creacion_panel_pvstate-db.md#vista-state)

### Automatizaciones

#### Encendido/Apagado

- [Apagar automáticamente el extractor de aire de Baño 1](config/automations/areas/banyo_1/apagar_automaticamente_s001.yaml)
- [Encender/Apagar extractor de aire de Baño 1](config/automations/areas/banyo_1/encender_apagar_s001_con_b004.yaml)
- [Encender/Apagar regleta TV de Dormitorio A](config/automations/areas/dormitorio_a/encender_apagar_p005_con_b003.yaml)
- [Encender/Apagar la luz de Dormitorio C](config/automations/areas/dormitorio_c/encender_apagar_l001_con_b001_b002.yaml)
- [Encender/Apagar regleta Impresora 3D de Dormitorio C](config/automations/areas/dormitorio_c/encender_apagar_p002_con_b001_b002.yaml)
- [Encender/Apagar regleta TV de Dormitorio C](config/automations/areas/dormitorio_c/encender_apagar_p001_con_b001_b002.yaml)

##### Energía

- [Cambiar tarifa eléctrica automáticamente según franja horaria](config/automations/system/cambiar_tarifa_electrica_automaticamente.yaml)

##### Avisos y notificaciones

- [Notificar llamada en la puerta principal de Pasillo 1](config/automations/areas/pasillo_1/notificar_llamada_puerta_principal_v001.yaml)
- [Enviar notificación actualización disponible para HomeAssistant](config/automations/system/notificar_actualizacion_disponible_ha.yaml)
- [Enviar notificación al apagar HomeAssistant](config/automations/system/notificar_apagado_ha.yaml)
- [Enviar notificación al iniciar HomeAssistant](config/automations/system/notificar_encendido_ha.yaml)
