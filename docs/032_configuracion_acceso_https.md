# Configuración del acceso usando HTTPS

- [Justificación del método utilizado](#justificación-del-método-utilizado)
- [Instalación del certificado en HomeAssistant](#instalación-del-certificado-en-homeassistant)
- [Configuración de la integración HTTP](#configuración-de-la-integración-http)
- [Reinicio de HomeAssistant](#reinicio-de-homeassistant)

## Justificación del método utilizado

Aunque existen otras maneras diferentes de configurar el acceso _HTTPS_ a la instancia de _HomeAsistant_, como puede ser [Let's encrypt](https://letsencrypt.org), esta configuración se basa en el uso de un certificado _SSL_ 'autofirmado', generado y firmado usando una _entidad certificadora_ propia.

El principal punto negativo de utilizar _Let's encrypt_, es que requiere tener expuestos a _Internet_ los puertos `80` y `443`. En el caso de un servidor prestando servicios en _Internet_ no representa mayor inconveniente, pero tratándose de una conexión doméstica no es la mejor opción.

En cambio, el mayor inconveniente que presenta la opción elegida, usar un certificado _autofirmado_, es el tener que instalar el certificado de la _entidad certificadora_ en los navegadores de todos los dispositivos que se vayan a conectar a la instancia de _HomeAssistant_, para que puedan reconocer el certificado que ésta les presenta.

## Instalación del certificado en HomeAssistant

- Una vez se dispone del certificado y de su correspondiente clave, es necesario copiarlos al directorio [ssl/](../config/ssl) de _HomeAssistant_, renombrando el certificado como `fullchain.pem` y la clave como `privkey.pem`.

  > **Importante**<br/>
  > Es muy importante que los permisos y propietarios de los ficheros sean los adecuados.

  ```shell
    ssl$ chmod 644 fullchain.pem
    ssl$ chmod 600 privkey.pem
  ```

## Configuración de la integración HTTP

- Dentro del directorio `integrations/` hay que crear el fichero [http.yaml](../config/integrations/http.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Acceso a HomeAssistant
    ##

    # https://www.home-assistant.io/integrations/http

    http:
      # Configuración del acceso mediante HTTPS.
      server_port: 8123
      ssl_certificate: "/config/ssl/fullchain.pem"
      ssl_key: "/config/ssl/privkey.pem"
      # Habilitar el filtrado de direcciones IP.
      ip_ban_enabled: true
      # Número de intentos fallidos de conexión para una dirección IP
      #  antes de ser bloqueada por el sistema.
      login_attempts_threshold: 5
  ```

## Reinicio de HomeAssistant

- Reiniciar _HomeAssistant_ para que se apliquen todos los cambios realizados. Una vez el sistema haya arrancado de nuevo, el acceso iya podrá hacerse utilizando el protocolo `https:\\`.
