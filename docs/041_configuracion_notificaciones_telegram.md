# Configuración de notificaciones en Telegram

- [Creación de un bot en Telegram](#creación-de-un-bot-en-telegram)
- [Integración: Telegram bot](#integración-telegram-bot)
- [Integración: Notify](#integración-notify)
  - [Creación de los servicios de notificaciones](#creación-de-los-servicios-de-notificaciones)
- [Creación de automatizaciones](#creación-de-automatizaciones)

## Creación de un bot en Telegram

- El primer paso es crear un _bot_:
  - Desde la aplicación de _Telegram_ hay que abrir un _chat_ con el usuario `@botfather` y pulsar en `/start`.
  - Teclear el comando `/newbot` e introducir un nombre (ejemplo: `Telegram Notifications`) y un usuario (ejemplo: `telegram_bot`) para el nuevo _bot_. El nombre del usuario tiene que acabar necesariamente en `bot`.
  - Guardar el _token_ que devuelve.
- El segundo paso es obtener el _ID_ de nuestra cuenta de _Telegram_:
  - Desde _Telegram_ hay que abrir un _chat_ con el usuario `@myidbot` y pulsar en `/start`.
  - Teclear el comando `/getid`.
  - Guardar el identificador que devuelve.
- Antes de empezar a enviarle notificaciones al _bot_, es necesario abrir un _chat_ con él y enviarle un mensaje desde nuestro propio usuario.

## Integración: Telegram bot

- Dentro del directorio `integrations/` hay que crear el fichero [telegram_bot.yaml](../config/integrations/telegram_bot.yaml) con el siguiente contenido. En él hay que incluir todos los identificadores de _Telegram_ que van a estar autorizados a comunicarse con el _bot_.

  ```yaml
    ---
    ##
    ## Integración con el bot de Telegram
    ##

    # https://www.home-assistant.io/integrations/telegram_bot

    # Permite la comunicación con bots de Telegram
    #  para el envío de notificaciones.
    - platform: polling
      api_key: !secret telegram_bot_token
      allowed_chat_ids:
        - !secret telegram_a_chat_id
        - !secret telegram_c_chat_id
  ```

- A continuación es necesario añadir la siguiente configuración al fichero [secrets.yaml](../config/secrets.template.yaml), junto a sus correspondientes valores.

  ```yaml
    ···
    ##
    ## Datos para autenticación de servicios externos
    ##

    telegram_bot_token: "${TOKEN_GENERADO}"
    telegram_a_chat_id: "${ID_DEL_USUARIO_EN_TELEGRAM}"
    telegram_c_chat_id: "${ID_DEL_USUARIO_EN_TELEGRAM}"
    ···
  ```

## Integración: Notify

- Dentro del directorio `integrations/` hay que crear el fichero [notify.yaml](../config/integrations/notify.yaml).

### Creación de los servicios de notificaciones

- Dentro del directorio `entities/notifiers/` hay que crear un fichero por cada usuario al que queramos notificar mediante el _bot_, con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Servicio de notificaciones para el bot de Telegram
    ##

    platform: telegram
    name: "${NOMBRE_PARA_LA_ENTIDAD_DEL_NOTIFICADOR}"
    # Identificador del chat establecido con el bot.
    chat_id: !secret ${CLAVE_CORRESPONDIENTE_AL_ID_DEL_USUARIO_A_NOTIFICAR}
  ```

## Creación de automatizaciones

- [Enviar notificación actualización disponible para HomeAssistant](config/automations/system/notificar_actualizacion_disponible_ha.yaml)
- [Enviar notificación al apagar HomeAssistant](../config/automations/system/notificar_apagado_ha.yaml)
- [Enviar notificación al iniciar HomeAssistant](../config/automations/system/notificar_encendido_ha.yaml)
