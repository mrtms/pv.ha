# Configuración de sensores de monitorización

- [Sensores de monitorización del propio equipo](#sensores-de-monitorización-del-propio-equipo)
  - [Integración: Systemmonitor](#integración-systemmonitor)
  - [Sensor: host_ipv4_address](#sensor-host_ipv4_address)
  - [Integración: Version](#integración-version)
- [Sensores de monitorización de la red](#sensores-de-monitorización-de-la-red)
  - [Integración: DNSip](#integración-dnsip)
  - [Integración: SNMP](#integración-snmp)
- [Sensores de información de periodos de tiempo](#sensores-de-información-de-periodos-de-tiempo)
  - [Template: date_month_day](#template-date_month_day)
  - [Template: date_month_num](#template-date_month_num)
  - [Template: date_month](#template-date_month)
  - [Template: date_week_day](#template-date_week_day)
  - [Template: date_year](#template-date_year)
  - [Template: is_last_day_of_month](#template-is_last_day_of_month)
  - [Template: is_night](#template-is_night)
  - [Template: is_night_num](#template-is_night_num)
  - [Template: is_weekend](#template-is_weekend)
- [Sensores varios](#sensores-varios)
  - [Sensor: hadb_size](#sensor-hadb_size)

## Sensores de monitorización del propio equipo

### Integración: Systemmonitor

- La integración se agrega y configura desde el propio interfaz gráfico. Las entidades que se van a utilizar son las siguientes:
  - sensor.system_monitor_disk_usage
  - sensor.system_monitor_last_boot
  - sensor.system_monitor_load_1m
  - sensor.system_monitor_load_15m
  - sensor.system_monitor_load_5m
  - sensor.system_monitor_memory_usage
  - sensor.system_monitor_processor_temperature
  - sensor.system_monitor_processor_use
  - sensor.system_monitor_swap_usage

#### Registro de la información de los sensores

- Para registrar en la base de datos la información de algunos de los sensores, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.

  ```yaml
    ---
    ···
      ···
      include:
        entity_globs:
          ···
          - sensor.load_*
          ···
        entities:
          ···
          - sensor.last_boot
          ···
  ```

### Sensor: host_ipv4_address

- Dentro del directorio `entities/command_line/` hay que crear el fichero [host_ipv4_address.yaml](../config/entities/command_line/host_ipv4_address.yaml) con el siguiente contenido, para definir un sensor que contenga la dirección _IP_ del `host` en el que se está ejecutando _HomeAsistant_.

  ```yaml
    ---
    ##
    ## Dirección IP del host
    ##

    # https://www.home-assistant.io/integrations/command_line

    - sensor:
        name: "Host IPv4 address"
        unique_id: host_ipv4_address
        command: "echo ${HOST_IPV4ADDR}"
  ```

### Integración: Version

- Sirve para definir sensores que monitorizan la versión instalada de _Home-Assistant_ y la versión más reciente que está disponible.
- La integración se agrega y configura desde el propio interfaz gráfico. Las entidades que se van a utilizar son las siguientes:
  - binary_sensor.docker_hub_update_available
  - sensor.current_version
  - sensor.docker_hub

#### Automatización: Notificación de actualización de HomeAssistant

- Dentro del directorio `automations/system/` hay que crear el fichero [notificar_actualizacion_disponible_ha.yaml](../config/automations/system/notificar_actualizacion_disponible_ha.yaml) para definir una automatización que avise de la existencia de una nueva versión de _HomeAssistant_ mediante una notificación de tipo persistente en el interfaz gráfico y un mensaje a través de _Telegram_.

## Sensores de monitorización de la red

### Integración: DNSip

- Sirve para definir un sensor que monitorice los cambios en la dirección IP externa de la red.
- La integración se agrega y configura desde el propio interfaz gráfico. Las entidades que se van a utilizar son las siguientes:
  - sensor.ipv4_external_address
- Como `host` para monitorizar utilizaremos un equipo que esté disponible en _Internet_ y que responda a `ICMP ping`.

#### Registro de la información de los sensores

- Para registrar en la base de datos la información del sensor, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir el sensor.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.ipv4_external_address
          ···
  ```

### Integración: SNMP

- Dentro del directorio `entities/sensors/` hay que crear el fichero [network_monitor.yaml](../config/entities/sensors/network_monitor.yaml) con el siguiente contenido, para definir una serie de sensores que monitoricen el estado de la red.

  ```yaml
    ---
    ##
    ## Monitorización de la red
    ##

    # https://www.home-assistant.io/integrations/snmp

    - platform: snmp
      name: "Host network in"
      unique_id: host_network_in
      host: !secret snmp_host_data_source
      version: 2c
      baseoid: .1.3.6.1.2.1.31.1.1.1.6.2
      scan_interval: 10

    - platform: derivative
      name: "Host network in derivative"
      source: sensor.host_network_in
      unit_time: s
      unit: B

    - platform: snmp
      name: "Host network out"
      unique_id: host_network_out
      host: !secret snmp_host_data_source
      version: 2c
      baseoid: .1.3.6.1.2.1.2.2.1.16.2
      scan_interval: 10

    - platform: derivative
      name: "Host network out derivative"
      source: sensor.host_network_out
      unit_time: s
      unit: B
  ```

#### Template: host_network_monitor

- Dentro del directorio `entities/templates/` hay que crear el fichero [host_network_monitor_template.yaml](../config/entities/templates/host_network_monitor_template.yaml) con el siguiente contenido, para definir un par de sensores que devuelvan el tráfico de red de entrada y salida del equipo que aloja _HomeAssistant_.

  ```yaml
    ---
    ##
    ## Plantilla: Tráfico de red del host en Mbps
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "Host network in Mbps"
          unique_id: host_network_in_mbps
          unit_of_measurement: Mbps
          state: >-
            {{  [ ( ( states('sensor.host_network_in_derivative') | float(none) * 8) / 1000000 ) | round(2, default=none), 0 ] | max  }}

        - name: "Host network out Mbps"
          unique_id: host_network_out_mbps
          unit_of_measurement: Mbps
          state: >-
            {{  [ ( ( states('sensor.host_network_out_derivative') | float(none) * 8 ) / 1000000 ) | round(2, default=none), 0 ] | max  }}
  ```

#### Modificaciones al fichero secrets.yaml

- También habrá que modificar el fichero [secrets.yaml](../config/secrets.template.yaml), para añadir la siguiente variable, junto a su correspondiente valor, dentro de la sección `Datos de red`.

  ```yaml
    ···
    snmp_host_data_source: "${IP_O_NOMBRE_DNS_DEL_SERVIDOR_SNMP_DEL_HOST_DE_HA}"
    ···
  ```

#### Registro de la información de los sensores

- Para registrar en la base de datos la información de algunos de los sensores, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.host_network_in_mbps
          - sensor.host_network_out_mbps
          ···
  ```

#### Registro de cambios de estado de los sensores

- Para evitar que se muestre en el registro de cambios de estado la información de algunos de los sensores, dentro del fichero [integrations/logbook.yaml](../config/integrations/logbook.yaml) hay que añadir una serie de exclusiones.

  ```yaml
    ---
    ···
      ···
      exclude:
        ···
        entity_globs:
          ···
          - sensor.host_network_*
          ···
        ···
  ```

## Sensores de información de periodos de tiempo

### Template: date_month_day

- Dentro del directorio `entities/templates/` hay que crear el fichero [date_month_day_template.yaml](../config/entities/templates/date_month_day_template.yaml) para definir un sensor que devuelva el día del mes.

### Template: date_month_num

- Dentro del directorio `entities/templates/` hay que crear el fichero [date_month_num_template.yaml](../config/entities/templates/date_month_num_template.yaml) para definir un sensor que devuelva el mes en formato numérico.

### Template: date_month

- Dentro del directorio `entities/templates/` hay que crear el fichero [date_month_template.yaml](../config/entities/templates/date_month_template.yaml) para definir un sensor que devuelva el nombre del mes.

### Template: date_week_day

- Dentro del directorio `entities/templates/` hay que crear el fichero [date_week_day_template.yaml](../config/entities/templates/date_week_day_template.yaml) para definir un sensor que devuelva el día de la semana.

### Template: date_year

- Dentro del directorio `entities/templates/` hay que crear el fichero [date_year_template.yaml](../config/entities/templates/date_year_template.yaml) para definir un sensor que devuelva el año.

### Template: is_last_day_of_month

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_last_day_of_month_template.yaml](../config/entities/templates/is_last_day_of_month_template.yaml) para definir un sensor que devuelva cuándo es el último día del mes.

### Template: is_night

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_night_template.yaml](../config/entities/templates/is_night_template.yaml) para definir un sensor que devuelva cuándo es de noche.

### Template: is_night_num

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_night_num_template.yaml](../config/entities/templates/is_night_num_template.yaml) para definir un sensor que devuelva cuándo es de noche, pero en formato numérico.

### Template: is_weekend

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_weekend_template.yaml](../config/entities/templates/is_weekend_template.yaml) para definir un sensor que devuelva cuándo es fin de semana.

#### Registro de la información de los sensores

- Para registrar en la base de datos la información que nos interesa, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los siguientes sensores.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.is_last_day_of_month
          - sensor.is_night_num
          - sensor.is_night
          - sensor.is_weekend
          ···
      ···
      exclude:
        ···
        entities:
          ···
          - sensor.date_month_day
          - sensor.date_month_num
          - sensor.date_month
          - sensor.date_week_day
          - sensor.date_year
          ···
  ```

## Sensores varios

### Sensor: hadb_size

- Dentro del directorio `entities/command_line/` hay que crear el fichero [hadb_size.yaml](../config/entities/command_line/hadb_size.yaml) con el siguiente contenido, para definir un sensor que monitorice el tamaño de la base de datos _SQLite_ de _HomeAsistant_.

  ```yaml
    ---
    ##
    ## Tamaño de la base de datos SQLite de HomeAssistant
    ##

    # https://www.home-assistant.io/integrations/command_line

    - sensor:
        name: "HADB size"
        unique_id: hadb_size
        command: "ls -ahl /config/home-assistant_v2.db | awk '{print substr($5, 1, length($5)-1)}'"
        unit_of_measurement: MB
        scan_interval: 43200
  ```
