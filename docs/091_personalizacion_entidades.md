# Personalizacion de entidades

Para personalizar las entidades que se han definido hasta el momento, es necesario crear una serie de ficheros que definan los valores que se desean mostrar.

## Dispositivos y entidades del sistema

- Dentro del directorio `customizations/entities/system/` hay que crear los siguientes ficheros:

  - [aemet.yaml](../config/customizations/entities/system/aemet.yaml)
  - [current_energy_tariff.yaml](../config/customizations/entities/system/current_energy_tariff.yaml)
  - [date_month_day.yaml](../config/customizations/entities/system/date_month_day.yaml)
  - [date_month.yaml](../config/customizations/entities/system/date_month.yaml)
  - [date_week_day.yaml](../config/customizations/entities/system/date_week_day.yaml)
  - [date_year.yaml](../config/customizations/entities/system/date_year.yaml)
  - [hadb_size.yaml](../config/customizations/entities/system/hadb_size.yaml)
  - [inside_average_humidity.yaml](../config/customizations/entities/system/inside_average_humidity.yaml)
  - [inside_average_temperature.yaml](../config/customizations/entities/system/inside_average_temperature.yaml)
  - [is_last_day_of_month.yaml](../config/customizations/entities/system/is_last_day_of_month.yaml)
  - [is_night.yaml](../config/customizations/entities/system/is_night.yaml)
  - [is_weekend.yaml](../config/customizations/entities/system/is_weekend.yaml)
  - [network_monitor.yaml](../config/customizations/entities/system/network_monitor.yaml)
  - [system_monitor.yaml](../config/customizations/entities/system/system_monitor.yaml)
  - [utility_meter.yaml](../config/customizations/entities/system/utility_meter.yaml)
  - [version.yaml](../config/customizations/entities/system/version.yaml)
  - [workday.yaml](../config/customizations/entities/system/workday.yaml)
  - [zigbee.yaml](../config/customizations/entities/system/zigbee.yaml)

- Dentro del directorio `customizations/entities/tests/` hay que crear los siguientes ficheros:

  - [l002.tests.yaml](../config/customizations/entities/tests/l002.tests.yaml)
  - [l003.tests.yaml](../config/customizations/entities/tests/l003.tests.yaml)
  - [v002.tests.yaml](../config/customizations/entities/tests/v002.tests.yaml)

## Dispositivos y entidades situadas en la zona principal

### Área: Baño 1

- Dentro del directorio `customizations/entities/areas/banyo_1/` hay que crear los siguientes ficheros:
  - [b004.servicio_1.yaml](../config/customizations/entities/areas/banyo_1/b004.banyo_1.yaml)
  - [s001.servicio_1.yaml](../config/customizations/entities/areas/banyo_1/s001.banyo_1.yaml)
  - [w008.servicio_1.yaml](../config/customizations/entities/areas/banyo_1/w008.banyo_1.yaml)

### Área: Baño 2

### Área: Cocina

- Dentro del directorio `customizations/entities/areas/cocina/` hay que crear los siguientes ficheros:
  - [f001.cocina.yaml](../config/customizations/entities/areas/cocina/f001.cocina.yaml)
  - [f002.cocina.yaml](../config/customizations/entities/areas/cocina/f002.cocina.yaml)

### Área: Despacho 1

- Dentro del directorio `customizations/entities/areas/despacho_1/` hay que crear los siguientes ficheros:
  - [w003.despacho_1.yaml](../config/customizations/entities/areas/despacho_1/w003.despacho_1.yaml)

### Área: Despacho 2

- Dentro del directorio `customizations/entities/areas/despacho_2/` hay que crear los siguientes ficheros:
  - [p004.despacho_2.yaml](../config/customizations/entities/areas/despacho_2/p004.despacho_2.yaml)
  - [w004.despacho_1.yaml](../config/customizations/entities/areas/despacho_2/w004.despacho_2.yaml)

### Área: Dormitorio A

- Dentro del directorio `customizations/entities/areas/dormitorio_a/` hay que crear los siguientes ficheros:
  - [b003.dormitorio_a.yaml](../config/customizations/entities/areas/dormitorio_a/b003.dormitorio_a.yaml)
  - [p005.dormitorio_a.yaml](../config/customizations/entities/areas/dormitorio_a/p005.dormitorio_a.yaml)
  - [w005.dormitorio_a.yaml](../config/customizations/entities/areas/dormitorio_a/w005.dormitorio_a.yaml)

### Área: Dormitorio B

- Dentro del directorio `customizations/entities/areas/dormitorio_b/` hay que crear los siguientes ficheros:
  - [w006.dormitorio_b.yaml](../config/customizations/entities/areas/dormitorio_b/w006.dormitorio_b.yaml)

### Área: Dormitorio C

- Dentro del directorio `customizations/entities/areas/dormitorio_c/` hay que crear los siguientes ficheros:
  - [b001.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/b001.dormitorio_c.yaml)
  - [b002.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/b002.dormitorio_c.yaml)
  - [c001.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/c001.dormitorio_c.yaml)
  - [eaton_ellipse_eco_1200va.yaml](../config/customizations/entities/areas/dormitorio_c/eaton_ellipse_eco_1200va.yaml)
  - [l001.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/l001.dormitorio_c.yaml)
  - [p001.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/p001.dormitorio_c.yaml)
  - [p002.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/p002.dormitorio_c.yaml)
  - [w001.dormitorio_c.yaml](../config/customizations/entities/areas/dormitorio_c/w001.dormitorio_c.yaml)

### Área: Pasillo 1

- Dentro del directorio `customizations/entities/areas/pasillo_1/` hay que crear los siguientes ficheros:
  - [m001.pasillo_1.yaml](../config/customizations/entities/areas/pasillo_1/m001.pasillo_1.yaml)
  - [oc002.pasillo_1.yaml](../config/customizations/entities/areas/pasillo_1/oc002.pasillo_1.yaml)
  - [oc003.pasillo_1.yaml](../config/customizations/entities/areas/pasillo_1/oc003.pasillo_1.yaml)
  - [v001.pasillo_1.yaml](../config/customizations/entities/areas/pasillo_1/v001.pasillo_1.yaml)

### Área: Pasillo 2

### Área: Salón

- Dentro del directorio `customizations/entities/areas/salon/` hay que crear los siguientes ficheros:
  - [p003.salon.yaml](../config/customizations/entities/areas/salon/p003.salon.yaml)
  - [w002.salon.yaml](../config/customizations/entities/areas/salon/w002.salon.yaml)

### Área: Terraza

- Dentro del directorio `customizations/entities/areas/terraza/` hay que crear los siguientes ficheros:
  - [oc001.terraza.yaml](../config/customizations/entities/areas/terraza/oc001.terraza.yaml)
  - [w007.terraza.yaml](../config/customizations/entities/areas/terraza/w007.terraza.yaml)
