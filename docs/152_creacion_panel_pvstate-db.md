# Creación de un panel: PVState-DB

- [Estructura del árbol de directorios](#estructura-del-árbol-de-directorios)
- [Definición del panel](#definición-del-panel)
- [Carga de los componentes del panel](#carga-de-los-componentes-del-panel)
- [Definición de vistas](#definición-de-vistas)
  - [Vista: State](#vista-state)

## Estructura del árbol de directorios

- Para recrear la base de la estructura de directorios que se va a utilizar en el panel, es necesario aplicar los siguientes comandos desde dentro del directorio `config/lovelace/` de _Home Assistant_.

  ```shell
    lovelace$ mkdir db-pvstate
    lovelace$ mkdir db-pvstate/{cards,views}
    lovelace$ mkdir db-pvstate/cards/state
  ```

## Definición del panel

- Dentro del directorio `config/lovelace/` hay que añadir la siguiente sección al fichero [dashboards.yaml](../config/lovelace/dashboards.yaml).

  ```yaml
    ···

    # Configuración para el panel de estados PVState
    db-pvstate:
      mode: yaml
      title: "State"
      icon: mdi:chart-box
      show_in_sidebar: true
      filename: lovelace/db-pvstate/dashboard.yaml
  ```

## Carga de los componentes del panel

- Dentro del directorio `db-pvstate/` hay que crear el fichero [dashboard.yaml](../config/lovelace/db-pvstate/dashboard.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Dashboard: db-pvstate
    ##

    title: "State"
    views: !include_dir_list ./views
  ```

## Definición de vistas

### Vista: State

![Vista estados](images/51_state_view_scr.png)

- Dentro del directorio `db-pvstate/views/` hay que crear el fichero [11_state.yaml](../config/lovelace/db-pvstate/views/11_state.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Estado
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista con un resumen de estados (para reemplazar al histórico despues de
    #   los cambios que hicieron en la versión 202207).

    title: "State"
    icon: mdi:chart-box
    path: state
    type: panel
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/state
  ```

#### Configuración de tarjetas de la vista

- Dentro del directorio `db-pvstate/cards/state/` hay que crear los siguientes ficheros:
  - [Panel de estado de entidades importantes](../config/lovelace/db-pvstate/cards/state/11_state.yaml)
