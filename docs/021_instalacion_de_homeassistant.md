# Instalación de HomeAssistant

- [Acerca de la instalación](#acerca-de-la-instalación)
- [Docker y herramientas relacionadas](#docker-y-herramientas-relacionadas)
  - [Instalación de Docker en modo rootless](#instalación-de-docker-en-modo-rootless)
  - [Instalación de Docker-Compose](#instalación-de-docker-compose)
- [Creación de la estructura para albergar los datos no volátiles](#creación-de-la-estructura-para-albergar-los-datos-no-volátiles)
- [Creación de la estructura de la red](#creación-de-la-estructura-de-la-red)
- [Creación de los contenedores](#creación-de-los-contenedores)
  - [Contenedor: Portainer](#contenedor-portainer)
  - [Contenedores de HA y servicios relacionados](#contenedores-de-ha-y-servicios-relacionados)
  - [Contenedores de MQTT](#contenedores-de-mqtt)

## Acerca de la instalación

La instalación de _HomeAssistant_, en su versión [container](https://www.home-assistant.io/installation/generic-x86-64#install-home-assistant-container), se va a llevar a cabo sobre una instalación mínima de _Debian 12 (bookworm) GNU/Linux_, utilizando para ello la imagen [netinstall](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.7.0-amd64-netinst.iso) para arquitectura _x86_ de 64 _bits_.

Detalles de la instalación de _Debian_:

- No se va a introducir una contraseña para el usuario `root`. De esta manera, el usuario que se creará posteriormente tendrá la configuración adecuada para poder ejecutar el comando `sudo`.
- El usuario creado durante la instalación aparecerá referenciado como `${USUARIO_UTILIZADO}` durante el resto del proceso de instalación y configuración. Aquellos comandos en los que el _prompt_ aparece especificado como `~$`, se ejecutarán con dicho usuario.
- Posteriormente se creará otro usuario específico para _Docker_ que aparecerá referenciado como `${USUARIO_DOCKER}` durante el resto del proceso de instalación y configuración. Aquellos comandos en los que el _prompt_ aparece especificado como `udocker:~$`, se ejecutarán con dicho usuario.
- A la hora de particionar el espacio en disco, hay que tener en cuenta que _Docker_ va a almacenar las imágenes y contenedores en la ruta `/var/lib/docker`, o en `/home/${USUARIO_DOCKER}/.local/share/docker` si se trata de una instalación en modo _rootless_, por lo que es necesario contar con el espacio necesario si se van a hacer múltiples particiones.
- Se seleccionarán únicamente los componentes `SSH server` y `standard system utilities` para su instalación.

## Docker y herramientas relacionadas

Uno de los requisitos necesarios para poder instalar esta versión de _HomeAssistant_, es tener instalada una versión reciente de _Docker_. También se va a instalar la herramienta _docker-compose_.

### Instalación de Docker en modo rootless

No necesita permisos de `root` para ejecutar el _Docker daemon_ ni los contenedores.

#### Requisitos necesarios

- Instalación del paquete `uidmap` que proporciona los comandos `newuidmap` y `newgidmap`.
- El usuario y grupo bajo los que se va a ejecutar _Docker_, han de contar con un rango de al menos 65535 _UID_/_GID_ especificado en los ficheros `/etc/subuid` y `/etc/subgid`.
- Instalación del paquete `dbus-user-session` si no se encuentra instalado.
- Se recomienda la instalación del paquete `fuse-overlayfs`.
- Instalación del paquete `slirp4netns` con un número de versión mayor que la `0.4.0`.

#### Creación de un usuario específico para ejecutar Docker

```shell
  ~$ sudo addgroup --gid ${GID_GRUPO_DOCKER} ${GRUPO_DOCKER}
  ~$ sudo adduser --uid ${UID_USUARIO_DOCKER} --ingroup ${GRUPO_DOCKER} ${USUARIO_DOCKER}
  ~$ sudo chmod -R o-rwx /home/${USUARIO_DOCKER}
  ~$ sudo adduser ${USUARIO_DOCKER} sudo
```

#### Instalación con el usuario creado

Para instalar _Docker_ en modo _rootless_, hay que seguir los pasos especificados a continuación.

```shell
  udocker:~$ curl -fsSL https://get.docker.com/rootless | sh
```

```conf
  [INFO] Installed docker.service successfully.
  [INFO] To control docker.service, run: `systemctl --user (start|stop|restart) docker.service`
  [INFO] To run docker.service on system startup, run: `sudo loginctl enable-linger ${USUARIO_DOCKER}`

  [INFO] Creating CLI context "rootless"
  Successfully created context "rootless"

  [INFO] Make sure the following environment variables are set (or add them to ~/.bashrc):

  export PATH=/home/${USUARIO_DOCKER}/bin:$PATH
  export DOCKER_HOST=unix:///run/user/${UID_USUARIO_DOCKER}/docker.sock
```

```shell
  udocker:~$ sudo loginctl enable-linger ${USUARIO_DOCKER}
```

Una vez terminada la instalación, es necesario añadir las siguientes líneas al final del fichero `.profile` del usuario `${USUARIO_DOCKER}`, sustituyendo `${USUARIO_DOCKER}` por el nombre del usuario y `${UID_USUARIO_DOCKER}` por el _UID_ de dicho usuario.

```bash
  # set variables for Docker
  export PATH=/home/${USUARIO_DOCKER}/bin:$PATH
  export DOCKER_HOST=unix:///run/user/${UID_USUARIO_DOCKER}/docker.sock
```

#### Limitar los recursos empleados por un contenedor

Para poder limitar el uso de recursos en un contenedor, utilizando parámetros como `--cpus`, `--memory`, `--pids-limit` en los comandos de `docker run`, es necesario estar utilizando `cgroup v2` y `systemd` en el sistema. Para comprobar si están disponibles, se puede utilizar el comando:

```shell
  udocker:~$ docker info
```

```conf
    ···
  Cgroup Driver: systemd
  Cgroup Version: 2
    ···
```

En el caso de que el valor devuelto para `Cgroup Driver` sea `systemd`, estará activado el control de recursos y se puede consultar qué controladores están disponibles para el usuario `${USUARIO_DOCKER}` mediante el comando:

```shell
  udocker:~$ cat /sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.controllers
```

```conf
  memory pids
```

Para ver la lista completa de los controladores de recursos disponibles a nivel del sistema, se puede utilizar el comando:

```shell
  udocker:~$ cat /sys/fs/cgroup/cgroup.controllers
```

```conf
  cpuset cpu io memory hugetlb pids rdma
```

Para modificar la lista de controladores disponibles para el usuario `${USUARIO_DOCKER}`, añadiendo además `cpu`, `cpuset` e `io`, es necesario ejecutar la siguiente serie de comandos:

```shell
  udocker:~$ sudo -i
  ~# mkdir -p /etc/systemd/system/user@.service.d
  ~# cat > /etc/systemd/system/user@.service.d/delegate.conf << EOF
     [Service]
     Delegate=cpu cpuset io memory pids
     EOF
  ~# systemctl daemon-reload
```

```shell
  udocker:~$ cat /sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.controllers
```

```conf
  cpuset cpu io memory pids
```

#### Exponer puertos inferiores a 1024

En caso de que haya que exponer puertos por debajo del 1024, es necesario realizar los pasos expuestos a continuación:

- Procedimiento habitual:
  - Añadir la línea `net.ipv4.ip_unprivileged_port_start=0` en el fichero `/etc/sysctl.conf` (o crear uno nuevo en `/etc/sysctl.d`).
  - Ejecutar `sudo sysctl --system` para recargar la configuración.
- Procedimiento alternativo para obtener el mismo resultado:

  ```shell
    udocker:~$ sudo setcap cap_net_bind_service=ep $(which rootlesskit)
    udocker:~$ systemctl --user restart docker
  ```

#### Habilitar ping

En algunas instalaciones es necesario realizar los siguientes pasos para que funcione el _ping_ entre los contenedores:

- Añadir la línea `net.ipv4.ping_group_range = 0 2147483647` en el fichero `/etc/sysctl.conf` (o crear uno nuevo en `/etc/sysctl.d`).
- Ejecutar `sudo sysctl --system` para recargar la configuración.

#### Eliminar al usuario docker del grupo sudo

```shell
  ~$ sudo deluser udocker sudo
```

### Instalación de Docker-Compose

En el caso de estar haciendo la instalación en un equipo con una arquitectura diferente, hay que buscar la versión adecuada para descargar en el sitio `https://github.com/docker/compose/releases/`.

```shell
  ~$ sudo curl -L 'https://github.com/docker/compose/releases/download/v2.29.2/docker-compose-linux-x86_64' -o /usr/local/bin/docker-compose
  ~$ sudo chmod +x /usr/local/bin/docker-compose
```

## Creación de la estructura para albergar los datos no volátiles

Se va a crear una estructura de directorios en la que los contenedores guardarán su información de configuración y datos. De esta manera, al tener toda esa información centralizada en un mismo lugar, será posible hacer las copias de seguridad de una manera mucho más cómoda.

### Almacén general de datos de contenedores

Creación del directorio y asignación de los permisos y propietarios para el directorio principal.

```shell
  ~$ sudo mkdir -p /srv/docker
  ~$ sudo chmod o-rwx /srv/docker
  ~$ sudo chown ${USUARIO_DOCKER}:${GRUPO_DOCKER} /srv/docker
```

#### Almacén de datos de Portainer

Creación del directorio y asignación de los permisos y propietarios para el directorio de _Portainer_.

```shell
  ~$ sudo mkdir -p /srv/docker/portainer-data/data
  ~$ sudo chmod -R ug+rwx,o-rwx /srv/docker/portainer-data
  ~$ sudo chown -R ${USUARIO_DOCKER}:${GRUPO_DOCKER} /srv/docker/portainer-data
```

Una vez ejecutado el contenedor, hay que ver con qué propietarios se han creado los ficheros y subdirectorios dentro, para corregir los propietarios del directorio padre y eliminar los permisos para el resto.

```shell
  ~$ sudo chmod -R o-rwx /srv/docker/portainer-data
```

#### Almacén de datos de NUT-UPSd

Creación del directorio y asignación de los permisos y propietarios para el directorio de _NUT-UPSd_.

```shell
  ~$ sudo mkdir -p /srv/docker/homeassistant-data/nut-upsd
  ~$ sudo sh -c "echo '${CONTRASEÑA_PARA_NUT-UPSD}' > /srv/docker/homeassistant-data/nut-upsd/nut-upsd-password"
  ~$ sudo chmod -R ug+rwX,o-rwx /srv/docker/homeassistant-data/nut-upsd
  ~$ sudo chown -R ${USUARIO_DOCKER}:${GRUPO_DOCKER} /srv/docker/homeassistant-data/nut-upsd
```

Una vez ejecutado el contenedor, hay que ver con qué propietarios se han creado los ficheros y subdirectorios dentro, para corregir los propietarios del directorio padre en caso de que sea necesario y eliminar los permisos para el resto.

```shell
  ~$ sudo chmod -R o-rwx /srv/docker/homeassistant-data/nut-upsd
```

#### Almacén de datos de HomeAssistant

Creación del directorio y asignación de los permisos y propietarios para el directorio de _HomeAssistant_.

```shell
  ~$ sudo mkdir -p /srv/docker/homeassistant-data/config
  ~$ sudo chmod ug+rwX,o-rwx /srv/docker/homeassistant-data/config
  ~$ sudo chown ${USUARIO_DOCKER}:${GRUPO_DOCKER} /srv/docker/homeassistant-data
```

Una vez ejecutado el contenedor, hay que ver con qué propietarios se han creado los ficheros y subdirectorios dentro, para corregir los propietarios del directorio padre en caso de que sea necesario y eliminar los permisos para el resto.

```shell
  ~$ sudo chmod -R o-rwx /srv/docker/homeassistant-data
```

#### Almacén de datos de MQTT y ZigBee2MQTT

Creación de los directorios y asignación de los permisos y propietarios para los directorios de _MQTT_ y _ZigBee2MQTT_.

```shell
  ~$ sudo mkdir -p /srv/docker/mqtt-data/mosquitto/{config,data,log}
  ~$ sudo chmod -R ug+rwX,o-rwx /srv/docker/mqtt-data/mosquitto
  ~$ sudo chown -R ${USUARIO_DOCKER}:${GRUPO_DOCKER} /srv/docker/mqtt-data/mosquitto
```

```shell
  ~$ sudo mkdir -p /srv/docker/mqtt-data/z2mqtt
  ~$ sudo chmod ug+rwX,o-rwx /srv/docker/mqtt-data/z2mqtt
  ~$ sudo chown ${USUARIO_DOCKER}:${GUPO_DOCKER} /srv/docker/mqtt-data/z2mqtt
```

Una vez ejecutados los contenedores, hay que ver con qué propietarios se han creado los ficheros y subdirectorios dentro, para corregir los propietarios de los directorios padre en caso de que sea necesario y eliminar los permisos para el resto.

```shell
  ~$ sudo chmod -R o-rwx /srv/docker/mqtt-data/mosquitto
  ~$ sudo chmod -R o-rwx /srv/docker/mqtt-data/z2mqtt
```

## Creación de la estructura de la red

Se va a organizar la configuración de red de los contenedores en tres redes diferentes. Una para el gestor de contenedores _Portainer_, otra para todos los contenedores relacionados con la instalación de _HomeAssistant_ y una última red para los contenedores relacionados con _MQTT_. En la configuración de los contenedores se especificarán las direcciones _IP_ estáticas correspondientes a la red en la que se encuentren.

Las configuraciones de red y direcciones _IP_ se han elegido por motivos de conveniencia y pueden cambiarse siempre que mantengan una cierta lógica que permita que la red sea funcional.

Aunque no es necesario hacerlo si se utilizan los ficheros `docker-compose.yml` de _stacks_ que se incluyen en el siguiente punto, a continuación se detallan 2 posibles maneras alternativas de crear las redes para los contenedores:

- Utilizando directamente los comandos de _Docker_. Tiene el inconveniente de que cada vez que se quieran crear, cambiar su estado o hacer una modificación, hay que volver a ejecutar los comandos completos, incluyendo todos sus parámetros. Se incluyen los comandos para poder crear cada red de manera independiente.
- Utilizando un fichero de `docker-compose`. Se pueden utilizar los ficheros `docker-compose.yml` directamente con la herramienta `docker-compose` o desde la herramienta _Portainer_. Se incluye el contenido del fichero `docker-compose.yml`.

### Usando comandos de Docker

```shell
  udocker:~$ docker network create  --driver=bridge  --subnet=10.36.100.0/24  --opt 'com.docker.network.enable_ipv6'='false' net_portainer
  udocker:~$ docker network create  --driver=bridge  --subnet=10.36.120.0/24  --opt 'com.docker.network.enable_ipv6'='false' net_ha
  udocker:~$ docker network create  --driver=bridge  --subnet=10.36.121.0/24  --opt 'com.docker.network.enable_ipv6'='false' net_mqtt
```

### Usando el comando docker-compose

```yaml
# networks.docker-compose.yml
version: "3"

networks:
  net_portainer:
    name: net_portainer
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
        - subnet: 10.36.100.0/24
  net_ha:
    name: net_ha
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
        - subnet: 10.36.120.0/24
  net_mqtt:
    name: net_mqtt
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
        - subnet: 10.36.121.0/24
```

```shell
  udocker:~$ docker-compose --file networks.docker-compose.yml up --detach
```

## Creación de los contenedores

A continuación se detallan 2 posibles maneras de crear y mantener los contenedores:

- Utilizando directamente los comandos de _Docker_. Tiene el inconveniente de que cada vez que se quiera crear un contenedor, cambiar su estado o hacer una modificación al mismo, hay que volver a ejecutar los comandos completos, incluyendo todos sus parámetros. Se incluyen los comandos para poder crear cada contenedor de manera independiente.
- Utilizando ficheros de `docker-compose`. La opción más cómoda para crear, gestionar y mantener los contenedores, pudiendo utilizar los ficheros `docker-compose.yml` directamente con la herramienta `docker-compose` o desde la herramienta _Portainer_.
  - Se incluye el contenido de los ficheros `docker-compose.yml` individuales para cada contendor. Requieren que se hayan creado previamente las correspondientes redes.
  - Se incluyen ficheros `docker-compose.yml` que agrupan la creación, configuración de la red y los contenedores (_stacks_).

### Contenedor: Portainer

Para acceder a la aplicación una vez creado el contenedor, es necesario especificar en un navegador la _url_ `https://${IP_O_NOMBRE_DNS_DEL_EQUIPO_DONDE_SE_HA_CREADO_EL_CONTENEDOR}:10443` o `https://localhost:10443` si el acceso se realiza desde el propio equipo en donde está el contenedor.

Fichero [docker-compose.yml](../misc/docker_configs/portainer-stack/docker-compose.yml) para crearlo como un _stack_, incluyendo la creación y configuración de la red. [`opción más recomendable`]

```shell
  udocker:~$ docker-compose --project-name=portainer-stack up --detach
```

#### Usando comandos de Docker

```shell
  udocker:~$ docker run  --name=portainer --detach=true  --restart=unless-stopped                   \
              --network=net_portainer  --ip=10.36.100.11  --publish=10443:9443                      \
              --security-opt='no-new-privileges:true'                                               \
              --mount type=bind,source=/etc/localtime,target=/etc/localtime,ro                      \
              --mount type=bind,source=/etc/timezone,target=/etc/timezone,ro                        \
              --mount type=bind,source=/${XDG_RUNTIME_DIR}/docker.sock,target=/var/run/docker.sock  \
              --mount type=bind,source=/srv/docker/portainer-data/data,target=/data                 \
              portainer/portainer-ce:2.21.0-alpine
```

#### Usando el comando docker-compose

```yaml
# portainer.docker-compose.yml
version: "3"

services:
  portainer:
    container_name: portainer
    hostname: portainer
    privileged: false
    restart: unless-stopped

    networks:
      net_portainer:
        ipv4_address: 10.36.100.11

    ports:
      - 10443:9443

    security_opt:
      - no-new-privileges

    volumes:
      - type: bind
        source: /etc/localtime
        target: /etc/localtime
        read_only: true
      - type: bind
        source: /etc/timezone
        target: /etc/timezone
        read_only: true
      - type: bind
        source: /${XDG_RUNTIME_DIR}/docker.sock
        target: /var/run/docker.sock
        read_only: true
      - type: bind
        source: /srv/docker/portainer-data/data
        target: /data

    image: portainer/portainer-ce:2.21.0-alpine
```

```shell
  udocker:~$ docker-compose --file portainer.docker-compose.yml up --detach
```

### Contenedores de HA y servicios relacionados

Fichero [docker-compose.yml](../misc/docker_configs/homeassistant-stack/docker-compose.yml) para crear todos los contenedores de servicios (_NUT-UPSd_, _HomeAssistant_) como un _stack_, incluyendo la creación y configuración de la red. Si se ha instalado _Portainer_, se puede usar este mismo fichero para crear el _stack_ desde su interfaz _web_. [`opción más recomendable`]

```shell
  udocker:~$ docker-compose --project-name=homeassistant-stack up --detach
```

#### Contenedor: NUT-UPSd

Para poder acceder al dispositivo de comunicación con el _SAI_ en modo _rootless_, es necesario cambiar los permisos del mismo. Para hacerlo de manera dinámica, ya que el dispositivo utilizado para la comunicación puede ir cambiando al enchufarlo y desenchufarlo, hay que crear una regla de _udev_ basada en el identificador de fabricante y de producto.

Hay que crear el fichero `/etc/udev/rules.d/10-eaton-ellipse-eco.rules` con la información que se obtiene con el comando `lsusb` para el dispositivo correspondiente.

```shell
  ~$ lsusb
     Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
     Bus 001 Device 002: ID 0463:ffff MGE UPS Systems UPS
     Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

```conf
  SUBSYSTEMS=="usb", ATTRS{idVendor}=="0463", ATTRS{idProduct}=="ffff", MODE="0666"
```

Una vez creado el fichero es necesario recargar la configuración del servicio, para lo que se puede emplear el siguiente comando.

```shell
  ~$ sudo systemctl restart systemd-udevd.service
```

Si en algún momento cambia el dispositivo utilizado para comunicarse con el _SAI_, habrá que actualizar la configuración del contenedor de _NUT-UPSd_ correspondiente, ya que en ese caso el contenedor no será capaz de seguir comunicándose con él.

##### Usando comandos de Docker

```shell
  udocker:~$ docker run  --name nut-upsd  --detach=true  --restart=unless-stopped                                         \
              --device=/dev/bus/usb/001/002:/dev/bus/usb/001/002                                                          \
              --env=['NAME=eaton']  --env=['DRIVER=usbhid-ups']  --env=['PORT=auto']  --env=['USER=nut']                  \
              --env=['SERIAL=${NUMSERIE_UPS}']  --env=['VENDORID=${ID_DEL_FABRICANTE}']  --env=['DEADTIME=25']            \
              --env=['MAXAGE=25']  --env=['POLLINTERVAL=12']                                                              \
              --network=net_ha  --ip=10.36.120.21  --publish=3493:3493                                                    \
              --security-opt='no-new-privileges:true'                                                                     \
              --mount type=bind,source=/etc/localtime,target=/etc/localtime,ro                                            \
              --mount type=bind,source=/etc/timezone,target=/etc/timezone,ro                                              \
              --mount type=bind,source=/dev/bus/usb/001/002,target=/dev/bus/usb/001/002                                   \
              --mount type=bind,source=/srv/docker/nut-upsd-data/nut-upsd-password,target=/run/secrets/nut-upsd-password  \
              instantlinux/nut-upsd:2.8.0-r4
```

##### Usando el comando docker-compose

```yaml
# nut.docker-compose.yml
version: "3"

services:
  nut-upsd:
    container_name: nut-upsd
    hostname: nut-upsd
    privileged: false
    restart: unless-stopped

    devices:
      - /dev/bus/usb/001/002:/dev/bus/usb/001/002

    environment:
      - "NAME=eaton"
      - "DRIVER=usbhid-ups"
      - "PORT=auto"
      - "USER=nut"
      - "SERIAL=${NUMSERIE_UPS}"
      - "VENDORID=${ID_DEL_FABRICANTE}"
      - "DEADTIME=25"
      - "MAXAGE=25"
      - "POLLINTERVAL=12"

    networks:
      net_ha:
        ipv4_address: 10.36.120.21

    ports:
      - 3493:3493

    security_opt:
      - no-new-privileges

    volumes:
      - type: bind
        source: /etc/localtime
        target: /etc/localtime
        read_only: true
      - type: bind
        source: /etc/timezone
        target: /etc/timezone
        read_only: true
      - type: bind
        source: /srv/docker/nut-upsd-data/nut-upsd-password
        target: /run/secrets/nut-upsd-password

    image: instantlinux/nut-upsd:2.8.0-r4
```

```shell
  udocker:~$ docker-compose --file nut.docker-compose.yml up --detach
```

#### Contenedor: HomeAssistant

Para acceder a la aplicación una vez creado el contenedor, es necesario especificar en un navegador la _url_ `http://${IP_O_NOMBRE_DNS_DEL_EQUIPO_DONDE_SE_HA_CREADO_EL_CONTENEDOR}:12123` o `http://localhost:12123` si el acceso se realiza desde el propio equipo en donde está el contenedor.

##### Usando comandos de Docker

```shell
  udocker:~$ docker run  --name=ha  --detach=true  --restart=unless-stopped                   \
              --network=net_ha  --ip=10.36.120.22  --publish=12123:8123                       \
              --security-opt='no-new-privileges:true'                                         \
              --mount type=bind,source=/etc/localtime,target=/etc/localtime,ro                \
              --mount type=bind,source=/etc/timezone,target=/etc/timezone,ro                  \
              --mount type=bind,source=/srv/docker/homeassistant-data/config,target=/config   \
              homeassistant/home-assistant:2024.8.3
```

##### Usando el comando docker-compose

```yaml
# ha.docker-compose.yml
version: "3"

services:
  homeassistant:
    container_name: homeassistant
    hostname: ha
    privileged: false
    restart: unless-stopped

    environment:
      - "HOST_NETWORK=192.168.231.0/24"
      - "HOST_IPADDR=192.168.231.36"

    networks:
      net_ha:
        ipv4_address: 10.36.120.22

    ports:
      - 12123:8123

    security_opt:
      - no-new-privileges

    volumes:
      - type: bind
        source: /etc/localtime
        target: /etc/localtime
        read_only: true
      - type: bind
        source: /etc/timezone
        target: /etc/timezone
        read_only: true
      - type: bind
        source: /srv/docker/homeassistant-data/config
        target: /config

    image: homeassistant/home-assistant:2024.8.3
```

```shell
  udocker:~$ docker-compose --file ha.docker-compose.yml up --detach
```

### Contenedores de MQTT

Fichero [docker-compose.yml](../misc/docker_configs/mqtt-stack/docker-compose.yml) para crear todos los contenedores de servicios (_MQTT_, _ZigBee2MQTT_) como un _stack_, incluyendo la creación y configuración de la red. Si se ha instalado _Portainer_, se puede usar este mismo fichero para crear el _stack_ desde su interfaz _web_. [`opción más recomendable`]

```shell
  udocker:~$ docker-compose --project-name=mqtt-stack up --detach
```

#### Contenedor: MQTT

##### Usando comandos de Docker

```shell
  udocker:~$ docker run  --name mqtt  --detach=true  --restart=unless-stopped                            \
              --network=net_mqtt  --ip=10.36.121.26  --publish=1883:1883  --publish=9001:9001            \
              --security-opt='no-new-privileges:true'                                                    \
              --mount type=bind,source=/etc/localtime,target=/etc/localtime,ro                           \
              --mount type=bind,source=/etc/timezone,target=/etc/timezone,ro                             \
              --mount type=bind,source=/srv/docker/mqtt-data/mosquitto/config,target=/mosquitto/config   \
              --mount type=bind,source=/srv/docker/mqtt-data/mosquitto/data,target=/mosquitto/data       \
              --mount type=bind,source=/srv/docker/mqtt-data/mosquitto/log,target=/mosquitto/log         \
              eclipse-mosquitto:2.0.18
```

##### Usando el comando docker-compose

```yaml
# mqtt.docker-compose.yml
version: "3"

services:
  mqtt:
    container_name: mqtt
    hostname: mqtt
    privileged: false
    restart: unless-stopped

    networks:
      net_mqtt:
        ipv4_address: 10.36.121.26

    ports:
      - 1883:1883
      - 9001:9001

    security_opt:
      - no-new-privileges

    volumes:
      - type: bind
        source: /etc/localtime
        target: /etc/localtime
        read_only: true
      - type: bind
        source: /etc/timezone
        target: /etc/timezone
        read_only: true
      - type: bind
        source: /srv/docker/mqtt-data/mosquitto/config
        target: /mosquitto/config
      - type: bind
        source: /srv/docker/mqtt-data/mosquitto/data
        target: /mosquitto/data
      - type: bind
        source: /srv/docker/mqtt-data/mosquitto/log
        target: /mosquitto/log

    image: eclipse-mosquitto:2.0.18
```

```shell
  ~$ docker-compose --file mqtt.docker-compose.yml up --detach
```

#### Contenedor: ZigBee2MQTT

Para acceder a la aplicación una vez creado el contenedor, es necesario especificar en un navegador la _url_ `http://${IP_O_NOMBRE_DNS_DEL_EQUIPO_DONDE_SE_HA_CREADO_EL_CONTENEDOR}:15080` o `http://localhost:15080` si el acceso se realiza desde el propio equipo en donde está el contenedor.

##### Usando comandos de Docker

```shell
  udocker:~$ docker run  --name=z2mqtt  --detach=true  --restart=unless-stopped        \
              --env=['Z2M_WATCHDOG=default']                                           \
              --network=net_mqtt  --ip=10.36.121.27  --publish=15080:8080              \
              --security-opt='no-new-privileges:true'                                  \
              --mount type=bind,source=/etc/localtime,target=/etc/localtime,ro         \
              --mount type=bind,source=/etc/timezone,target=/etc/timezone,ro           \
              --mount type=bind,source=/srv/docker/mqtt-data/z2mqtt,target=/app/data   \
              koenkk/zigbee2mqtt:1.39.1
```

##### Usando el comando docker-compose

```yaml
# z2mqtt.docker-compose.yml
version: "3"

services:
  z2mqtt:
    container_name: z2mqtt
    hostname: z2mqtt
    privileged: false
    restart: unless-stopped
    user: 1000:1000

    depends_on:
      mqtt:
        condition: service_healthy

    environment:
      - "Z2M_WATCHDOG=default"

    labels:
      autoheal: "true"

    networks:
      net_mqtt:
        ipv4_address: 10.36.121.27

    ports:
      - 15080:8080

    security_opt:
      - no-new-privileges

    volumes:
      - type: bind
        source: /etc/localtime
        target: /etc/localtime
        read_only: true
      - type: bind
        source: /etc/timezone
        target: /etc/timezone
        read_only: true
      - type: bind
        source: /srv/docker/mqtt-data/z2mqtt
        target: /app/data

    image: koenkk/zigbee2mqtt:1.39.1
```

```shell
  udocker:~$ docker-compose --file z2mqtt.docker-compose.yml up --detach
```
