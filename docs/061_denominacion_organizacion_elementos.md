# Denominación y organización de elementos

- [Definición de la zona principal](#definición-de-la-zona-principal)
  - [División de la zona en áreas específicas](#división-de-la-zona-en-áreas-específicas)
- [Relación de elementos y atributos utilizados](#relación-de-elementos-y-atributos-utilizados)
  - [Tipos de elementos](#tipos-de-elementos)
  - [Fabricantes](#fabricantes)
  - [Protocolos utilizados](#protocolos-utilizados)
- [Denominación de los dispositivos y entidades](#denominación-de-los-dispositivos-y-entidades)
  - [Dispositivos](#dispositivos)
  - [Entidades pertenecientes a dispositivos](#entidades-pertenecientes-a-dispositivos)

## Definición de la zona principal

- Esta zona reemplaza a la que genera automáticamente _HomeAssistant_ en el mapa.
- Dentro del directorio `entities/zones/`, hay que crear el fichero [home.yaml](../config/entities/zones/home.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Definición de la zona 'Home'
    ##

    name: "Home"
    icon: mdi:home-outline
    latitude: !secret home_latitude
    longitude: !secret home_longitude
    radius: 8
  ```

- También es necesario modificar el contenido del fichero [secrets.yaml](../config/secrets.template.yaml), añadiendo los valores correspondientes a las variables empleadas en el fichero de definición de la zona.

  ```yaml
    ···
    home_latitude: "${LATITUD_ZONA_HOME}"
    home_longitude: "${LONGITUD_ZONA_HOME}"
    ···
  ```

### División de la zona en áreas específicas

- La definición de las áreas no puede realizarse mediante ficheros `.yaml`, si no que ha de hacerse a través del interfaz _web_, accediendo al menú `Configuration` >> `Areas`.
- Áreas a definir:
  - Baño 1
  - Baño 2
  - Cocina
  - Despacho 1
  - Despacho 2
  - Dormitorio A
  - Dormitorio B
  - Dormitorio C
  - Pasillo 1
  - Pasillo 2
  - Salón
  - System
  - Terraza
  - Tests
- Para poder organizar las automatizaciones dentro de las áreas definidas, hay que crear los siguientes subdirectorios dentro del directorio `automations/`.

  ```shell
    ~$ cd config
    config$ mkdir -p automations/areas/banyo_1
    config$ mkdir -p automations/areas/banyo_2
    config$ mkdir -p automations/areas/cocina
    config$ mkdir -p automations/areas/despacho_1
    config$ mkdir -p automations/areas/despacho_2
    config$ mkdir -p automations/areas/dormitorio_a
    config$ mkdir -p automations/areas/dormitorio_b
    config$ mkdir -p automations/areas/dormitorio_c
    config$ mkdir -p automations/areas/pasillo_1
    config$ mkdir -p automations/areas/pasillo_2
    config$ mkdir -p automations/areas/salon
    config$ mkdir -p automations/areas/terraza
    config$ mkdir -p automations/system
    config$ mkdir -p automations/tests
  ```

- Para poder clasificar las personalizaciones de entidades dentro de las áreas definidas, hay que crear los siguientes subdirectorios dentro del directorio `customizations/entities/`.

  ```shell
    ~$ cd config
    config$ mkdir -p customizations/entities/areas/banyo_1
    config$ mkdir -p customizations/entities/areas/banyo_2
    config$ mkdir -p customizations/entities/areas/cocina
    config$ mkdir -p customizations/entities/areas/despacho_1
    config$ mkdir -p customizations/entities/areas/despacho_2
    config$ mkdir -p customizations/entities/areas/dormitorio_a
    config$ mkdir -p customizations/entities/areas/dormitorio_b
    config$ mkdir -p customizations/entities/areas/dormitorio_c
    config$ mkdir -p customizations/entities/areas/pasillo_1
    config$ mkdir -p customizations/entities/areas/pasillo_2
    config$ mkdir -p customizations/entities/areas/salon
    config$ mkdir -p customizations/entities/areas/terraza
    config$ mkdir -p customizations/entities/system
    config$ mkdir -p customizations/entities/tests
  ```

## Relación de elementos y atributos utilizados

### Tipos de elementos

| Elemento   | Código | Descripción                                         |
| ---------- | :----: | --------------------------------------------------- |
| AirQ       |   aq   | Sensores de calidad del aire                        |
| Button     |   b    | Botones y pulsadores                                |
| Cube       |   c    | Cubos mágicos                                       |
| Flood      |   f    | Sensores de inundación                              |
| Lightbulb  |   l    | Bombillas                                           |
| Meter      |   m    | Medidores de consumo                                |
| Open/Close |   oc   | Sensores de apertura y cierre de puertas y ventanas |
| Plug       |   p    | Enchufes                                            |
| Switch     |   s    | Interruptores y relés                               |
| Thermostat |   t    | Termostatos                                         |
| Vibration  |   v    | Sensores de vibración                               |
| Weather    |   w    | Sensores de humedad, presión y temperatura          |

### Fabricantes

| Nombre      | Código |
| ----------- | :----: |
| AirGradient |   ag   |
| Aqara       |   aq   |
| BlitzWolf   |   bw   |
| Livarno     |   lv   |
| Osram       |   or   |
| Shelly      |   sh   |
| Tuya        |   ty   |

### Protocolos utilizados

| Nombre    | Código |
| --------- | :----: |
| BlueTooth |   bt   |
| Matter    |   mt   |
| WiFi      |   wf   |
| ZigBee    |   zb   |
| Z-Wave    |   zw   |

## Denominación de los dispositivos y entidades

### Dispositivos

- ${_TIPO_DISPOSITIVO_} ${_CÓDIGO_FABRICANTE_} ${_CÓDIGO_PROTOCOLO_} ${_NUM_DISPOSITIVO: ###_}
  > Ejemplo:
  >
  > `Flood AQ ZB 001` - primer sensor de inundación, que usa el protocolo _ZigBee_ y es fabricado por _Aqara_

### Entidades pertenecientes a dispositivos

- Las capacidades de la entidad hay que copiarlas del nombre predeterminado de la misma. En el caso de no incluirlas, hay veces en que _HomeAssistant_ parece que no es capaz de detectarlas por si mismo.
- ${_TIPO_ENTIDAD_} \. ${_TIPO_DISPOSITIVO_} \_ ${_CÓDIGO_FABRICANTE_} \_ ${_CÓDIGO_PROTOCOLO_} \_ ${_NUM_DISPOSITIVO: ###_} \_ ${_CAPACIDADES_ENTIDAD_}
  > Ejemplo:
  >
  > `binary_sensor.flood_aq_zb_001_ias_zone` - sensor de tipo binario, del primer sensor de inundación, que usa el protocolo _ZigBee_ y es fabricado por _Aqara_
