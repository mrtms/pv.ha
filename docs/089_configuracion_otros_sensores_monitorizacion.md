# Configuración de otros sensores de monitorización

- [Sensor de monitorización de baterías](#sensor-de-monitorización-de-baterías)
  - [Group: batteries_grp](#group-batteries_grp)
  - [Template: are_batteries_working](#template-are_batteries_working)
- [Sensor de monitorización de dispositivos IP](#sensor-de-monitorización-de-dispositivos-ip)
  - [Binary sensor: are_ip_devices_working](#binary-sensor-are_ip_devices_working)
  - [Template: are_ip_devices_working](#template-are_ip_devices_working)
- [Sensor de monitorización de dispositivos ZigBee](#sensor-de-monitorización-de-dispositivos-zigbee)
  - [Group: zigbee_grp](#group-zigbee_grp)
  - [Template: are_zb_devices_working](#template-are_zb_devices_working)
- [Sensor de monitorización de Internet](#sensor-de-monitorización-de-internet)
  - [Template: is_internet_working](#template-is_internet_working)
- [Sensor de monitorización de la red WiFi](#sensor-de-monitorización-de-la-red-wifi)
  - [Template: is_wifi_working](#template-is_wifi_working)

## Sensor de monitorización de baterías

### Group: batteries_grp

- Dentro del directorio `entities/groups/` hay que crear el fichero [batteries_group.yaml](../config/entities/groups/batteries_group.yaml) con el siguiente contenido, para definir un grupo con todas las entidades de las que se va a monitorizar el estado.

  ```yaml
    ---
    ##
    ## Grupo: dispositivos a baterías
    ##

    # https://www.home-assistant.io/integrations/group

    batteries_grp:
      name: "batteries_grp"
      entities:
        - sensor.flood_aq_zb_001_power
        - sensor.flood_aq_zb_002_power
        - sensor.openclose_aq_zb_001_power
        - sensor.openclose_aq_zb_002_power
        - sensor.openclose_aq_zb_003_power
        - sensor.switch_aq_zb_001_power
        - sensor.switch_aq_zb_002_power
        - sensor.switch_aq_zb_003_power
        - sensor.switch_aq_zb_004_power
        - sensor.vibration_aq_zb_001_power
        - sensor.vibration_aq_zb_002_power
        - sensor.weather_aq_zb_001_power
        - sensor.weather_aq_zb_002_power
        - sensor.weather_aq_zb_003_power
        - sensor.weather_aq_zb_004_power
        - sensor.weather_aq_zb_005_power
        - sensor.weather_aq_zb_006_power
        - sensor.weather_aq_zb_007_power
        - sensor.weather_aq_zb_008_power
  ```

### Template: are_batteries_working

- Dentro del directorio `entities/templates/` hay que crear el fichero [are_batteries_working_template.yaml](../config/entities/templates/are_batteries_working_template.yaml) con el siguiente contenido, para definir un sensor que monitorice el estado de las baterías de los distintos dispositivos de la instalación de _Home Assistant_.

  ```yaml
    ---
    ##
    ## Plantilla: están operativas las baterías de los dispositivos
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "are_dev_batteries_ok"
          unique_id: are_dev_batteries_ok
          state: >-
            {%  set _group = 'group.batteries_grp'  %}
            {%  set _devices = expand(_group) | selectattr('state', '<=', '10')
                  | list | count  %}

            {%  if ( states('sensor.eaton_battery_charge') | int <= 30 )
                  or  ( _devices | int > 0 )  %}
                off
            {%  else  %}
                on
            {%  endif  %}
  ```

#### Registro de la información de los sensores

- Para registrar en la base de datos la información del sensor, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir el sensor.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.are_dev_batteries_ok
          ···
  ```

## Sensor de monitorización de dispositivos IP

### Binary sensor: are_ip_devices_working

- Dentro del directorio `entities/binary_sensors/` hay que crear el fichero [are_ip_devices_working.yaml](../config/entities/binary_sensors/are_ip_devices_working.yaml) con el siguiente contenido, para definir diversos sensores que monitoricen si los dispositivos _IP_ siguen operativos.

  ```yaml
    ---
    ##
    ## Responden los equipos a ping
    ##

    # https://www.home-assistant.io/integrations/ping

    - platform: ping
      name: "is_host_cloudflare_up"
      host: "1.1.1.1"
      count: 5
      scan_interval: 3600

    - platform: ping
      name: "is_host_google_up"
      host: "8.8.8.8"
      count: 5
      scan_interval: 3600

    - platform: ping
      name: "is_host_c57d_up"
      host: "c57d.penguin-village.lan"
      count: 5
      scan_interval: 60

    - platform: ping
      name: "is_host_ncc1701a_up"
      host: "ncc1701a.penguin-village.lan"
      count: 5
      scan_interval: 60

    - platform: ping
      name: "is_host_shellyem_up"
      host: "shellyem.penguin-village.lan"
      count: 5
      scan_interval: 300

    - platform: ping
      name: "is_host_yamato_up"
      host: !secret ha_dyndns_host
      count: 5
      scan_interval: 60

    - platform: ping
      name: "is_host_zigbee_coordinator_up"
      host: "silverc-zb01.penguin-village.lan"
      count: 5
      scan_interval: 60
  ```

### Template: are_ip_devices_working

- Dentro del directorio `entities/templates/` hay que crear el fichero [are_ip_devices_working_template.yaml](../config/entities/templates/are_ip_devices_working_template.yaml) con el siguiente contenido, para definir un sensor que monitorice el estado de ciertos sensores de dispositivos _IP_.

  ```yaml
    ---
    ##
    ## Plantilla: están operativos los dispositivos IP
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "are_dev_ip_devices_up"
          unique_id: are_dev_ip_devices_up
          state: >-
            {%  if ( is_state('binary_sensor.is_host_c57d_up', 'on')
                     and  is_state('binary_sensor.is_host_ncc1701a_up', 'on')
                     and  is_state('binary_sensor.is_host_shellyem_up', 'on')
                     and  is_state('binary_sensor.is_host_zigbee_coordinator_up', 'on') )  %}
                on
            {%  else  %}
                off
            {%  endif  %}
  ```

##### Registro de la información de los sensores

- Para registrar en la base de datos la información del sensor, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir el sensor.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.are_dev_ip_devices_up
          ···
  ```

## Sensor de monitorización de dispositivos ZigBee

### Group: zigbee_grp

- Dentro del directorio `entities/groups/` hay que crear el fichero [zigbee_group.yaml](../config/entities/groups/zigbee_group.yaml) con el siguiente contenido, para definir un grupo con todas las entidades de las que se va a monitorizar el estado.

  ```yaml
    ---
    ##
    ## Grupo: dispositivos ZigBee
    ##

    # https://www.home-assistant.io/integrations/group

    zigbee_grp:
      name: "zigbee_grp"
      entities:
        - sensor.flood_aq_zb_001_basic_rssi
        - sensor.flood_aq_zb_002_basic_rssi
        - sensor.lightbulb_lv_zb_001_basic_rssi
        - sensor.openclose_aq_zb_001_basic_rssi
        - sensor.openclose_aq_zb_002_basic_rssi
        - sensor.openclose_aq_zb_003_basic_rssi
        - sensor.plug_bw_zb_001_basic_rssi
        - sensor.plug_bw_zb_002_basic_rssi
        - sensor.plug_bw_zb_003_basic_rssi
        - sensor.plug_bw_zb_004_basic_rssi
        - sensor.plug_bw_zb_005_basic_rssi
        - sensor.switch_aq_zb_001_basic_rssi
        - sensor.switch_aq_zb_002_basic_rssi
        - sensor.switch_aq_zb_003_basic_rssi
        - sensor.switch_aq_zb_004_basic_rssi
        - sensor.switch_cn_zb_001_rssi
        - sensor.vibration_aq_zb_001_basic_rssi
        - sensor.vibration_aq_zb_002_basic_rssi
        - sensor.weather_aq_zb_001_basic_rssi
        - sensor.weather_aq_zb_002_basic_rssi
        - sensor.weather_aq_zb_003_basic_rssi
        - sensor.weather_aq_zb_004_basic_rssi
        - sensor.weather_aq_zb_005_basic_rssi
        - sensor.weather_aq_zb_006_basic_rssi
        - sensor.weather_aq_zb_007_basic_rssi
        - sensor.weather_aq_zb_008_basic_rssi
  ```

### Template: are_zb_devices_working

- Dentro del directorio `entities/templates/` hay que crear el fichero [are_zb_devices_working_template.yaml](../config/entities/templates/are_zb_devices_working_template.yaml) con el siguiente contenido, para definir un sensor que monitorice el estado de ciertos sensores de dispositivos _ZigBee_.

  ```yaml
    ---
    ##
    ## Plantilla: están operativos los dispositivos ZigBee
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "are_dev_zb_devices_up"
          unique_id: are_dev_zb_devices_up
          state: >-
            {%  set _group = 'group.zigbee_grp'  %}
            {%  set _devices = expand(_group)
                  | selectattr('state', '==', 'unavailable')
                  | list | count  %}

            {%  if is_state('binary_sensor.is_host_zigbee_coordinator_up', 'off')
                  or  ( _devices | int > 0 )  %}
                off
            {%  else  %}
                on
            {%  endif  %}
  ```

##### Registro de la información de los sensores

- Para registrar en la base de datos la información del sensor, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir el sensor.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.are_dev_zb_devices_up
          ···
  ```

## Sensor de monitorización de Internet

### Template: is_internet_working

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_internet_working_template.yaml](../config/entities/templates/is_internet_working_template.yaml) con el siguiente contenido, para definir un sensor que monitorice el estado de la conexión a _Internet_.

  ```yaml
    ---
    ##
    ## Plantilla: está operativa la conexión a Internet
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "is_svc_internet_up"
          unique_id: is_svc_internet_up
          state: >-
            {%  if ( is_state('binary_sensor.is_host_yamato_up', 'on')
                     or  is_state('binary_sensor.is_host_cloudflare_up', 'on')
                     or  is_state('binary_sensor.is_host_google_up', 'on') )  %}
                on
            {%  else  %}
                off
            {%  endif  %}
  ```

## Sensor de monitorización de la red WiFi

### Template: is_wifi_working

- Dentro del directorio `entities/templates/` hay que crear el fichero [is_wifi_working_template.yaml](../config/entities/templates/is_wifi_working_template.yaml) con el siguiente contenido, para definir un sensor que monitorice el estado de la red _WiFi_.

  ```yaml
    ---
    ##
    ## Plantilla: está operativa la red WiFi
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    - sensor:
        - name: "is_svc_wifi_up"
          unique_id: is_svc_wifi_up
          state: >-
            {%  if ( is_state('binary_sensor.is_host_shellyem_up', 'on')
                     or  is_state('binary_sensor.is_host_c57d_up', 'on')
                     or  is_state('binary_sensor.is_host_ncc1701a_up', 'on') )  %}
                on
            {%  else  %}
                off
            {%  endif  %}
  ```
