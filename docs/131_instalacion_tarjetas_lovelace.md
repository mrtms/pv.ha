# Instalación de tarjetas para Lovelace

- [Tarjeta: auto entities card](#tarjeta-auto-entities-card)
- [Tarjeta: battery state card](#tarjeta-battery-state-card)
- [Tarjeta: button card](#tarjeta-button-card)
- [Tarjeta: layout card](#tarjeta-layout-card)
- [Tarjeta: mini graph card](#tarjeta-mini-graph-card)
- [Tarjeta: mod card](#tarjeta-mod-card)
- [Tarjeta: pvpc hourly pricing card](#tarjeta-pvpc-hourly-pricing-card)
- [Tarjeta: vertical stack in card](#tarjeta-vertical-stack-in-card)
- [Tarjeta: weather card](#tarjeta-weather-card)

## Tarjeta: auto entities card

- [Repositorio en GitHub](https://github.com/thomasloven/lovelace-auto-entities)
- Dentro del directorio `www/community/` hay que crear el directorio [auto-entities-card/](../config/www/community/auto-entities-card).

  ```shell
    config$ mkdir www/community/auto-entities-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    auto-entities-card$ wget https://github.com/thomasloven/lovelace-auto-entities/archive/refs/tags/1.13.0.tar.gz
    auto-entities-card$ tar xfz v1.13.0.tar.gz
    auto-entities-card$ mv lovelace-auto-entities-1.13.0/auto-entities.js .
    auto-entities-card$ rm -rf lovelace-auto-entities-1.13.0
    auto-entities-card$ rm -f v1.13.0.tar.gz
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/auto-entities-card/auto-entities.js?v=1.12.1
          type: module
    ```

## Tarjeta: battery state card

- [Repositorio en GitHub](https://github.com/maxwroc/battery-state-card)
- Dentro del directorio `www/community/` hay que crear el directorio [battery-state-card/](../config/www/community/battery-state-card).

  ```shell
    config$ mkdir www/community/battery-state-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    battery-state-card$ wget https://github.com/maxwroc/battery-state-card/releases/download/v3.2.1/battery-state-card.js
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/battery-state-card/battery-state-card.js?v=3.2.1
          type: module
    ```

## Tarjeta: button card

- [Repositorio en GitHub](https://github.com/custom-cards/button-card)
- Dentro del directorio `www/community/` hay que crear el directorio [button-card/](../config/www/community/button-card).

  ```shell
    config$ mkdir www/community/button-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    button-card$ wget https://github.com/custom-cards/button-card/releases/download/v4.1.2/button-card.js
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/button-card/button-card.js?v=4.1.2
          type: module
    ```

## Tarjeta: layout card

- [Repositorio en GitHub](https://github.com/thomasloven/lovelace-layout-card)
- Dentro del directorio `www/community/` hay que crear el directorio [layout-card/](../config/www/community/layout-card).

  ```shell
    config$ mkdir www/community/layout-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    layout-card$ wget https://github.com/thomasloven/lovelace-layout-card/archive/refs/tags/2.4.5.tar.gz
    layout-card$ tar xfz 2.4.5.tar.gz
    layout-card$ mv lovelace-layout-card-2.4.5/layout-card.js .
    layout-card$ rm -rf lovelace-layout-card-2.4.5
    layout-card$ rm -f 2.4.5.tar.gz
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/layout-card/layout-card.js?v=2.4.5
          type: module
    ```

## Tarjeta: mini graph card

- [Repositorio en GitHub](https://github.com/kalkih/mini-graph-card)
- Dentro del directorio `www/community/` hay que crear el directorio [mini-graph-card/](../config/www/community/mini-graph-card).

  ```shell
    config$ mkdir www/community/mini-graph-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    mini-graph-card$ wget https://github.com/kalkih/mini-graph-card/releases/download/v0.12.1/mini-graph-card-bundle.js
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/mini-graph-card/mini-graph-card-bundle.js?v=0.12.1
          type: module
    ```

## Tarjeta: mod card

- [Repositorio en GitHub](https://github.com/thomasloven/lovelace-card-mod)
- Dentro del directorio `www/community/` hay que crear el directorio [mod-card/](../config/www/community/mod-card).

  ```shell
    config$ mkdir www/community/mod-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    mod-card$ wget https://github.com/thomasloven/lovelace-card-mod/archive/refs/tags/3.4.3.tar.gz
    mod-card$ tar xfz 3.4.3.tar.gz
    mod-card$ mv lovelace-card-mod-3.4.3/card-mod.js .
    mod-card$ rm -rf lovelace-card-mod-3.4.3
    mod-card$ rm -f 3.4.3.tar.gz
  ```

- A partir de la versión 3.1 del módulo, se recomienda instalarlo como módulo de la integración `frontend`:

  - En el fichero `integrations/frontend.yaml` hay que añadir una sección nueva para módulos y añadirlo dentro de ella, reiniciando _Home Assistant_ a continuación.

    ```yaml
      frontend:
        ···
        extra_module_url:
          - /local/community/mod-card/card-mod.js
    ```

- Para versiones anteriores hay que añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/mod-card/card-mod.js?v=3.4.3
          type: module
    ```

## Tarjeta: pvpc hourly pricing card

- [Repositorio en GitHub](https://github.com/danimart1991/pvpc-hourly-pricing-card)
- Dentro del directorio `www/community/` hay que crear el directorio [pvpc-hourly-pricing-card/](../config/www/community/pvpc-hourly-pricing-card).

  ```shell
    config$ mkdir www/community/pvpc-hourly-pricing-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    pvpc-hourly-pricing-card$ wget https://github.com/danimart1991/pvpc-hourly-pricing-card/releases/download/1.15.0/pvpc-hourly-pricing-card.js
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/pvpc-hourly-pricing-card/pvpc-hourly-pricing-card.js?v=1.15.0
          type: module
    ```

## Tarjeta: vertical stack in card

- [Repositorio en GitHub](https://github.com/ofekashery/vertical-stack-in-card)
- Dentro del directorio `www/community/` hay que crear el directorio [vertical-stack-in-card/](../config/www/community/vertical-stack-in-card).

  ```shell
    config$ mkdir www/community/vertical-stack-in-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    vertical-stack-in-card$ wget https://raw.githubusercontent.com/ofekashery/vertical-stack-in-card/master/vertical-stack-in-card.js
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/vertical-stack-in-card/vertical-stack-in-card.js?v=0.4.4
          type: module
    ```

## Tarjeta: weather card

- [Repositorio en GitHub](https://github.com/bramkragten/weather-card)
- Dentro del directorio `www/community/` hay que crear el directorio [weather-card/](../config/www/community/weather-card).

  ```shell
    config$ mkdir www/community/weather-card
  ```

- Descargar el código de la tarjeta al directorio recién creado.

  ```shell
    weather-card$ wget https://github.com/bramkragten/weather-card/archive/refs/tags/v1.5.0.tar.gz
    weather-card$ tar xfz v1.5.0.tar.gz
    weather-card$ mv weather-card-1.5.0/dist/icons .
    weather-card$ mv weather-card-1.5.0/dist/weather-card*.js .
    weather-card$ rm -rf weather-card-1.5.0
    weather-card$ rm -f v1.5.0.tar.gz
  ```

- Añadir una entrada de recurso:

  - Desde el menú `Configuration` >> `Dashboards` >> `Resources` cuando el modo configurado para _Lovelace_ es `storage`.
  - En el fichero `integrations/lovelace.yaml`, dentro de la sección para componentes de terceros, cuando el modo configurado para _Lovelace_ es `yaml`.

    ```yaml
      resources:
        ···
        - url: /local/community/weather-card/weather-card.js?v=1.5.0
          type: module
    ```
