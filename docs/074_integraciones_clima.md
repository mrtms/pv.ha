# Integraciones de clima e información meteorológica

- [AEMET](#aemet)
  - [Integración: AEMET OpenData](#integración-aemet-opendata)
- [Sensores de humedad, presión y temperatura](#sensores-de-humedad-presión-y-temperatura)
  - [Obtención de la humedad y temperatura medias](#obtención-de-la-humedad-y-temperatura-medias)

## AEMET

### Integración: AEMET OpenData

- Valores para configurar la integración _AEMET OpenData_:
  - `API key`: ${_CLAVE_API_EN_UNA_ÚNICA_LÍNEA_} (es necesario solicitarla en la dirección `https://opendata.aemet.es/centrodedescargas/altaUsuario`)
  - `Name of the integration`: `AEMET`
  - `Latitude`: ${_LATITUD_DE_LA_ZONA_HOME_}
  - `Longitude`: ${_LONGITUD_DE_LA_ZONA_HOME_}

## Sensores de humedad, presión y temperatura

### Obtención de la humedad y temperatura medias

#### Sensor de humedad media de un grupo de sensores

- Una vez integrados y configurados los sensores de humedad, para crear uno que refleje la media de los valores obtenidos por dichos sensores, dentro del directorio `entities/sensors/` hay que crear el fichero [inside_average_humidity.yaml](../config/entities/sensors/inside_average_humidity.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Humedad media de los sensores de humedad interiores
    ##

    # https://www.home-assistant.io/integrations/min_max

    - platform: min_max
      name: "Inside average humidity"
      type: mean
      round_digits: 1
      entity_ids:
        - sensor.weather_aq_zb_001_humidity
        - sensor.weather_aq_zb_002_humidity
        - sensor.weather_aq_zb_003_humidity
        - sensor.weather_aq_zb_004_humidity
        - sensor.weather_aq_zb_005_humidity
        - sensor.weather_aq_zb_006_humidity
  ```

#### Sensor de temperatura media de un grupo de sensores

- Una vez integrados y configurados los sensores de temperatura, para crear uno que refleje la media de los valores obtenidos por dichos sensores, dentro del directorio `entities/sensors/` hay que crear el fichero [inside_average_temperature.yaml](../config/entities/sensors/inside_average_temperature.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Temperatura media de los sensores de temperatura interiores
    ##

    # https://www.home-assistant.io/integrations/min_max

    - platform: min_max
      name: "Inside average temperature"
      type: mean
      round_digits: 1
      entity_ids:
        - sensor.weather_aq_zb_001_temperature
        - sensor.weather_aq_zb_002_temperature
        - sensor.weather_aq_zb_003_temperature
        - sensor.weather_aq_zb_004_temperature
        - sensor.weather_aq_zb_005_temperature
        - sensor.weather_aq_zb_006_temperature
  ```
