# Configuración de registro y consulta de información

- [Configuración de integraciones](#configuración-de-integraciones)
  - [Integración: Logbook](#integración-logbook)
  - [Integración: Logger](#integración-logger)
  - [Integración: Recorder](#integración-recorder)

## Configuración de integraciones

### Integración: Logbook

- Dentro del directorio `integrations/` hay que modificar el fichero [logbook.yaml](../config/integrations/logbook.yaml) para dejarlo de la siguiente manera.

  ```yaml
    ---
    ##
    ## Consulta de cambios
    ##

    # https://www.home-assistant.io/integrations/logbook
    #
    # Integración habilitada de manera predeterminada en 'default_config:'.

    # Consulta de los cambios producidos, mostrándolos
    #  en orden cronológico inverso.
    logbook:
  ```

### Integración: Logger

- Dentro del directorio `integrations/` hay que crear el fichero [logger.yaml](../config/integrations/logger.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Registro de operaciones del sistema
    ##

    # https://www.home-assistant.io/integrations/logger

    # Registra las operaciones realizadas y los
    #  mensajes del sistema en los ficheros de registro.
    logger:
      # Nivel de registro (critical|fatal|error|warning|warn|info|debug|notset).
      default: warning
  ```

### Integración: Recorder

- Para que se almacene la información correspondiente a un sensor, es necesario incluirlo explícitamente en la configuración, asegurándose de que no exista ningún filtro que haga que sea ignorado.
- Dentro del directorio `integrations/` hay que crear el fichero [recorder.yaml](../config/integrations/recorder.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Grabación de cambios de estado en la base de datos
    ##

    # https://www.home-assistant.io/integrations/recorder

    # Permite seleccionar qué información se registra
    #  en la base de datos.
    recorder:
      auto_purge: true
      purge_keep_days: 7
      commit_interval: 4

      include:
        domains:
          - automation
  ```
