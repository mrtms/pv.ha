# Configuración inicial

- [Creación de directorios y ficheros necesarios](#creación-de-directorios-y-ficheros-necesarios)
- [Integración: Lovelace](#integración-lovelace)

## Creación de directorios y ficheros necesarios

- Dentro del directorio `config/` hay que crear el directorio [lovelace/](../config/lovelace).

  ```shell
    config$ mkdir lovelace
  ```

- Dentro del directorio `lovelace/` hay que crear el fichero [dashboards.yaml](../config/lovelace/dashboards.yaml) con el siguiente contenido.

  ```yaml
  ---
  ---
  ##
  ## Dashboards
  ##

  # https://www.home-assistant.io/lovelace
  # https://www.home-assistant.io/lovelace/dashboards-and-views/#dashboards
  ```

- Dentro del directorio `config/` hay que crear el directorio [www/community/](../config/www/community).

  ```shell
    config$ mkdir -p www/community
  ```

## Integración: Lovelace

- Dentro del directorio `integrations/` hay que crear el fichero [lovelace.yaml](../config/integrations/lovelace.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Lovelace
    ##

    # https://www.home-assistant.io/lovelace
    # https://www.home-assistant.io/lovelace/dashboards-and-views

    lovelace:
      # Modo predeterminado para gestionar Lovelace desde el
      #  propio interfaz gráfico.
      #mode: storage
      # Modo para gestionar Lovelace desde ficheros .yaml.
      #  Es necesario para que funcionen los componentes de
      #  terceros incluidos en este fichero.
      mode: yaml

      # Componentes de terceros instalados
      resources:


      # Inclusión de la configuración para paneles adicionales
      #  realizados en YAML.
      dashboards: !include ../lovelace/dashboards.yaml
  ```
