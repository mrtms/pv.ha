# Integración de dispositivos Shelly

- [Integración Shelly](#integración-shelly)
  - [Configuración de dispositivos Shelly](#configuración-de-dispositivos-shelly)

## Integración: Shelly

### Configuración de dispositivos Shelly

- Desde el menú `Configuration` >> `Devices & services` >> `Integrations` es necesario añadir la integración `Shelly` por cada dispositivo que queramos añadir.
- Valores para configurar la integración _Shelly_:
  - `Host`: ${_IP_O_NOMBRE_DNS_DISPOSITIVO_SHELLY_}
  - `Username`: ${_USUARIO_DISPOSITIVO_SHELLY_}
  - `Password`: ${_CONTRASEÑA_DISPOSITIVO_SHELLY_}
    > **Importante**<br/>
    > En caso de configurar el dispositivo _Shelly_ utilizando _CoIoT_, hay que hacerlo como _unicast_, especificando la dirección _IP_ y el puerto _CoIoT_ de la instancia de _Home Assistant_.
