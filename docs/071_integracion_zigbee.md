# Integración de dispositivos Zigbee

- [Integraciones de MQTT y ZigBee2MQTT](#integraciones-de-mqtt-y-zigbee2mqtt)
  - [Configuración de MQTT](#configuración-de-mqtt)
  - [Configuración de ZigBee2MQTT](#configuración-de-zigbee2mqtt)
  - [Configuración para el SLZB06M ZigBee Bridge en ZigBee2MQTT](#configuración-para-el-slzb06m-zigbee-bridge-en-zigbee2mqtt)
- [Dispositivos ZigBee](#dispositivos-zigbee)

## Integraciones de MQTT y ZigBee2MQTT

- Desde el menú `Configuration` >> `Devices & services` >> `Integrations` es necesario añadir la integración `MQTT`. Para su configuración hay que utilizar los siguientes valores:
  - `Broker`: ${_IP_O_NOMBRE_DNS_DEL_EQUIPO_DONDE_SE_HA_CREADO_EL_CONTENEDOR_}
  - `Port`: ${_PUERTO_EN_EL_QUE_ESCUCHA_EL_SERVICIO_}
  - `Username`: ${_NOMBRE_DEL_USUARIO_PARA_LA_AUTENTICACIÓN_}
  - `Password`: ${_CONTRASEÑA_PARA_EL_USUARIO_}

### Configuración de MQTT

- La configuración del fichero `mosquitto.conf` que se está utilizando en el contenedor para dicho servicio es la siguiente:

  ```conf
    # MQTT data persistence
    persistence true
    persistence_location /mosquitto/data/

    # MQTT logging
    log_type subscribe
    log_type unsubscribe
    log_type websockets
    log_type error
    log_type warning
    log_type notice
    log_type information
    log_dest file /mosquitto/log/mosquitto.log
    log_dest stdout

    # Enable settings per listener
    per_listener_settings true

    # MQTT default listener with authentication
    listener 1883 0.0.0.0
    password_file /mosquitto/config/a12npass_file
    allow_anonymous false

    # MQTT without authentication for healthcheck
    listener 1880 127.0.0.1
    allow_anonymous true

    # MQTT over WebSockets
    listener 9001 0.0.0.0
    protocol websockets
  ```

### Configuración de ZigBee2MQTT

- La configuración del fichero `configuration.yaml` que se está utilizando en el contenedor para dicho servicio es la siguiente:

  ```conf
    # Optional: allow new devices to join.
    permit_join: false

    # Docker-compose makes the MQTT server available using
    # the "mqtt" hostname.
    mqtt:
      # Required: MQTT server URL (use mqtts:// for SSL/TLS connection).
      server: 'mqtt://mqtt:1883'
      # Optional: MQTT base topic for Zigbee2MQTT MQTT messages (default: zigbee2mqtt).
      base_topic: 'zigbee2mqtt'
      # Optional: MQTT server authentication user (default: nothing).
      user: umqtt
      # Optional: MQTT server authentication password (default: nothing).
      password: '!secrets.yaml password'
      # Optional: Include device information to mqtt messages (default: false).
      include_device_information: true
      # Optional: MQTT keepalive in seconds (default: 60).
      keepalive: 60
      # Optional: MQTT protocol version (default: 4), set this to 5 if you use the
      # 'retention' device specific configuration.
      version: 4

    serial:
      # Required: location of the adapter (e.g. CC2531).
      # USB adapters - use format "port: /dev/ttyACM0".
      # To autodetect the USB port, set 'port: null'.
      # Ethernet adapters - use format "port: tcp://192.168.1.12:6638".
      port: 'tcp://${DIRECCIÓN_IP_O_NOMBRE_DEL_DISPOSITIVO_EN_LA_RED}:6638'
      # Optional: disable LED of the adapter if supported (default: false).
      disable_led: false
      # Optional: adapter type, specify if you are experiencing startup problems (default:
      # shown below, options: zstack, deconz, ember, zigate).
      adapter: ember
      # Optional: Baud rate speed for serial port, this can be anything firmware support but
      # default is 115200 for Z-Stack and EZSP, 38400 for Deconz, however note that some EZSP
      # firmware need 57600.
      baudrate: 115200
      # Optional: RTS/CTS Hardware Flow Control for serial port (default: false).
      rtscts: false

    # Will run frontend on port 8080.
    frontend: true

    homeassistant:
      # Optional: Home Assistant discovery topic (default: shown below)
      # Note: should be different from [MQTT base topic](../mqtt.md) to prevent errors in HA software
      discovery_topic: 'homeassistant'
      # Optional: Home Assistant status topic (default: shown below)
      # Note: in addition to the `status_topic`, 'homeassistant/status' will also be used
      status_topic: 'hass/status'
      # Optional: Home Assistant legacy entity attributes, (default: true), when enabled:
      # Zigbee2MQTT will send additional states as attributes with each entity. For example,
      # A temperature & humidity sensor will have 2 entities for the temperature and
      # humidity, with this setting enabled both entities will also have
      # an temperature and humidity attribute.
      # Note: Disabling this option, requires a Home Assistant restart
      legacy_entity_attributes: false
      # Optional: Home Assistant legacy triggers (default: true), when enabled:
      # - Zigbee2mqt will send an empty 'action' or 'click' after one has been send
      # - A 'sensor_action' and 'sensor_click' will be discoverd
      legacy_triggers: false

    # Note: all options are optional
    availability:
      active:
        # Time after which an active device will be marked as offline in
        # minutes (default = 10 minutes).
        timeout: 10
      passive:
        # Time after which a passive device will be marked as offline in
        # minutes (default = 1500 minutes aka 25 hours).
        timeout: 1500

    # Define the files which contains the configs.
    devices: devices.yaml
    groups: groups.yaml

    device_options:
      legacy: false

    advanced:
      # Optional: state caching, MQTT message payload will contain all attributes, not only
      # changed ones.
      # Has to be true when integrating via Home Assistant (default: true).
      cache_state: true
      # Optional: persist cached state, only used when cache_state: true (default: true).
      cache_state_persistent: true
      # Optional: send cached state on startup, only used when cache_state_persistent: true
      # (default: true).
      cache_state_send_on_startup: true

      # Optional: Add an elapsed attribute to MQTT messages, contains milliseconds since the
      # previous msg (default: false).
      elapsed: false
      # Optional: Add a last_seen attribute to MQTT messages, contains date/time of last
      # Zigbee message, possible values are: disable (default), ISO_8601, ISO_8601_local,
      # epoch (default: disable).
      last_seen: 'disable'

      # Optional: disables the legacy api (default: true)
      legacy_api: false
      # Whether to use legacy mode for the availability message payload (default: true).
      # true = online/offline.
      # false = {"state":"online"} / {"state":"offline"}.
      legacy_availability_payload: false

      # Optional: Logging level, options: debug, info, warning, error (default: info).
      log_level: info
      # Optional: Set log levels for certain namespaces hierarchies (default: {}).
      log_namespaced_levels:
        z2m:mqtt: warning
      # Optional: log timestamp format (default: 'YYYY-MM-DD HH:mm:ss')
      timestamp_format: 'YYYYMMDD:HHmmss'
      # Optional: Location of log directory (default: data/log/%TIMESTAMP%)
      log_directory: 'data/log'
      # Optional: Log file name, can also contain timestamp, e.g.: zigbee2mqtt_%TIMESTAMP%.log
      # (default: log.txt)
      log_file: '%TIMESTAMP%.log'
      # Optional: Rotate log every 10MB around 3 files (default: true).
      log_rotation: true
      # Optional: Output location of the log (default: shown below), leave empty to suppress
      # logging (log_output: []).
      # possible options: 'console', 'file', 'syslog'.
      log_output:
        - console
        - file
      # Create a symlink called "current" in the log directory which points to the latests log
      # directory. (default: false).
      log_symlink_current: false

      # Optional: MQTT output type: json, attribute or attribute_and_json (default: shown
      # below).
      # Examples when 'state' of a device is published:
      #  json: topic: 'zigbee2mqtt/my_bulb' payload '{"state": "ON"}'.
      #  attribute: topic 'zigbee2mqtt/my_bulb/state' payload 'ON".
      #  attribute_and_json: both json and attribute (see above).
      output: 'json'

      # Optional: Transmit power setting in dBm (default: 5).
      # This will set the transmit power for devices that bring an inbuilt amplifier.
      # It can't go over the maximum of the respective hardware and might be limited by
      # firmware (for example to migrate heat, or by using an unsupported firmware).
      # For the CC2652R(B) this is 5 dBm, CC2652P/CC1352P-2 20 dBm.
      transmit_power: 10

      # Optional: network encryption key, GENERATE will make Zigbee2MQTT generate a new
      # network key on next startup.
      # Note: changing requires re-pairing of all devices (default: shown below).
      network_key: GENERATE
      # Optional: ZigBee pan ID (default: shown below).
      # Setting pan_id: GENERATE will make Zigbee2MQTT generate a new panID on next startup.
      # Note: changing requires re-pairing of all devices (default: shown below).
      pan_id: GENERATE
      # Optional: Zigbee extended pan ID, GENERATE will make Zigbee2MQTT generate a new
      # extended panID on next startup.
      # Note: changing requires re-pairing of all devices (default: shown below).
      ext_pan_id: GENERATE
      # Optional: Zigbee channel, changing might require re-pairing of some devices (see docs
      # below). (Note: use a ZLL channel: 11, 15, 20, or 25 to avoid problems).
      # (default: 11).
      channel: 11
  ```

### Configuración para el SLZB06M ZigBee Bridge en ZigBee2MQTT

- Dentro de la configuración de _ZigBee2MQTT_, hay una serie de parámetros que tienen que ver con el coordinador _ZigBee_ que se va a emplear y que en este caso concreto se trata del [SLZB06M](https://smlight.tech/manual/slzb-06/). A continuación se detallan los parámetros concretos de la configuración de _ZigBee2MQTT_ específicos para dicho coordinador:

  ```conf
    serial:
      # Required: location of the adapter (e.g. CC2531).
      # USB adapters - use format "port: /dev/ttyACM0".
      # To autodetect the USB port, set 'port: null'.
      # Ethernet adapters - use format "port: tcp://192.168.1.12:6638".
      port: 'tcp://${DIRECCIÓN_IP_O_NOMBRE_DEL_DISPOSITIVO_EN_LA_RED}:6638'
      # Optional: disable LED of the adapter if supported (default: false).
      disable_led: false
      # Optional: adapter type, specify if you are experiencing startup problems (default:
      # shown below, options: zstack, deconz, ember, zigate).
      adapter: ember
      # Optional: Baud rate speed for serial port, this can be anything firmware support but
      # default is 115200 for Z-Stack and EZSP, 38400 for Deconz, however note that some EZSP
      # firmware need 57600.
      baudrate: 115200
      # Optional: RTS/CTS Hardware Flow Control for serial port (default: false).
      rtscts: false
  ```

  ```conf
    advanced:
      # Optional: Transmit power setting in dBm (default: 5).
      # This will set the transmit power for devices that bring an inbuilt amplifier.
      # It can't go over the maximum of the respective hardware and might be limited by
      # firmware (for example to migrate heat, or by using an unsupported firmware).
      # For the CC2652R(B) this is 5 dBm, CC2652P/CC1352P-2 20 dBm.
      transmit_power: 10
  ```

## Dispositivos ZigBee

- Los dispositivos _Zigbee_ se incorporan a _HomeAssistant_ desde el interfaz de administración de _ZigBee2MQTT_. Trás haber añadido un nuevo dispositivo, hay que renombrarlo con el nombre adecuado y activar la casilla `Update HomeAssistant entity ID` para que el dispositivo esté disponible desde _HomeAssistant_ con el nombre elegido.

### Registro de la información de los sensores

- Para registrar en la base de datos información de algunos de los sensores de los dispositivos, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.

  ```yaml
    ---
    ···
      include:
        entity_globs:
          ···
          - binary_sensor.flood_*
          - binary_sensor.openclose_*
          - binary_sensor.vibration_*
          - sensor.weather_*
          ···
        ···
      exclude:
        entity_globs:
          ···
          - sensor.*_active_power
          - sensor.*_backlight_mode
          - sensor.*_power_factor
          - sensor.*_power_on_state
          - sensor.*_rms_current
          - sensor.*_rms_voltage
          - sensor.*_summation_delivered
          - sensor.weather_*_power
          - sensor.weather_*_pressure
          ···
        ···
  ```
