# Creación de un panel: PV-DB

- [Estructura del árbol de directorios](#estructura-del-árbol-de-directorios)
- [Definición del panel](#definición-del-panel)
- [Carga de los componentes del panel](#carga-de-los-componentes-del-panel)
- [Definición de vistas](#definición-de-vistas)
  - [Vista: Home](#vista-home)
  - [Vista: Host](#vista-host)
  - [Vista: Network](#vista-network)
  - [Vista: Sensor](#vista-sensor)
  - [Vista: Energy](#vista-energy)
  - [Vista: Automation](#vista-automation)

## Estructura del árbol de directorios

- Para recrear la base de la estructura de directorios que se va a utilizar en el panel, es necesario aplicar los siguientes comandos desde dentro del directorio `config/lovelace/` de _Home Assistant_.

  ```shell
    lovelace$ mkdir db-pv
    lovelace$ mkdir db-pv/{cards,views}
    lovelace$ mkdir db-pv/cards/{automation,energy,home,host,network,sensor}
  ```

## Definición del panel

- Dentro del directorio `config/lovelace/` hay que añadir la siguiente sección al fichero [dashboards.yaml](../config/lovelace/dashboards.yaml).

  ```yaml
    ···

    # Configuración para el panel de PV
    db-pv:
      mode: yaml
      title: "PV"
      icon: mdi:view-dashboard
      show_in_sidebar: true
      filename: lovelace/db-pv/dashboard.yaml
  ```

## Carga de los componentes del panel

- Dentro del directorio `db-pv/` hay que crear el fichero [dashboard.yaml](../config/lovelace/db-pv/dashboard.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Dashboard: db-pv
    ##

    title: "PV"
    views: !include_dir_list ./views
  ```

## Definición de vistas

### Vista: Home

![Vista principal resumen](images/11_home_view_scr.png)

- Dentro del directorio `db-pv/views/` hay que crear el fichero [11_home.yaml](../config/lovelace/db-pv/views/11_home.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Inicio
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista con un resumen de la información más utilizada.

    title: "Home"
    icon: mdi:home
    path: home
    type: custom:vertical-layout

    badges: []
    cards: !include_dir_merge_list ../cards/home
  ```

##### Tarjetas de terceros utilizadas en la vista

- [Auto entities card](131_instalacion_tarjetas_lovelace.md#tarjeta-auto-entities-card)
- [Button card](131_instalacion_tarjetas_lovelace.md#tarjeta-button-card)
- [Layout card](131_instalacion_tarjetas_lovelace.md#tarjeta-layout-card)
- [Mini graph card](131_instalacion_tarjetas_lovelace.md#tarjeta-mini-graph-card)
- [Mod card](131_instalacion_tarjetas_lovelace.md#tarjeta-mod-card)
- [PVPC hourly pricing card](131_instalacion_tarjetas_lovelace.md#tarjeta-pvpc-hourly-pricing-card)
- [Weather card](131_instalacion_tarjetas_lovelace.md#tarjeta-weather-card)

##### Configuración de tarjetas de la vista

- Dentro del directorio `db-pv/cards/home/` hay que crear los siguientes ficheros:
  - [Cabecera del bloque de estado de la demanda de electricidad](../config/lovelace/db-pv/cards/home/11_a_header_status.yaml)
  - [Bloque de estado de la demanda de electricidad](../config/lovelace/db-pv/cards/home/11_b_status.yaml)
  - [Cabecera del bloque de avisos](../config/lovelace/db-pv/cards/home/12_a_header_warnings.yaml)
  - [Bloque de avisos](../config/lovelace/db-pv/cards/home/12_b_warnings.yaml)
  - [Cabecera del bloque de alertas](../config/lovelace/db-pv/cards/home/13_a_header_alerts.yaml)
  - [Bloque de alertas](../config/lovelace/db-pv/cards/home/13_b_alerts.yaml)
  - [Bloque de alertas de dispositivos IP](../config/lovelace/db-pv/cards/home/13_c_alerts_ip_devices.yaml)
  - [Bloque de alertas de dispositivos](../config/lovelace/db-pv/cards/home/13_d_alerts_unavailable.yaml)
  - [Bloque de alertas de estado de baterías](../config/lovelace/db-pv/cards/home/13_e_alerts_batteries.yaml)
  - [Cabecera del bloque de activación y desactivación de dispositivos](../config/lovelace/db-pv/cards/home/14_a_header_turn_on_off_devices.yaml)
  - [Bloque de activación y desactivación de dispositivos](../config/lovelace/db-pv/cards/home/14_b_turn_on_off_devices.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/home/19_layout_break.yaml)
  - [Cabecera del bloque de humedad y temperatura](../config/lovelace/db-pv/cards/home/21_a_header_humidity_temperature.yaml)
  - [Bloque de humedad y temperatura](../config/lovelace/db-pv/cards/home/21_b_humidity_temperature.yaml)
  - [Cabecera del bloque de predicción meteorológica](../config/lovelace/db-pv/cards/home/22_a_header_weather.yaml)
  - [Bloque de predicción meteorológica](../config/lovelace/db-pv/cards/home/22_b_weather.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/home/29_layout_break.yaml)
  - [Cabecera del bloque PVPC](../config/lovelace/db-pv/cards/home/31_a_header_pvpc.yaml)
  - [Bloque PVPC](../config/lovelace/db-pv/cards/home/31_b_pvpc.yaml)
  - [Cabecera del bloque de consumo de energía eléctrica](../config/lovelace/db-pv/cards/home/32_a_header_energy_consumption.yaml)
  - [Bloque de consumo de energía eléctrica](../config/lovelace/db-pv/cards/home/32_b_energy_consumption.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/home/39_layout_break.yaml)

### Vista: Host

![Vista de información del equipo de Home Assistant](images/21_host_view_scr.png)

- Dentro del directorio `db-pv/views/` hay que crear el fichero [21_host.yaml](../config/lovelace/db-pv/views/21_host.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Equipo
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista con información relacionada con el equipo que alberga
    #  la instalación de Home Assistant.

    title: "Host"
    path: host
    icon: mdi:monitor-dashboard
    type: custom:vertical-layout
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/host
  ```

##### Tarjetas de terceros utilizadas en la vista

- [Battery state card](131_instalacion_tarjetas_lovelace.md#tarjeta-battery-state-card)
- [Layout card](131_instalacion_tarjetas_lovelace.md#tarjeta-layout-card)
- [Mini graph card](131_instalacion_tarjetas_lovelace.md#tarjeta-mini-graph-card)
- [Mod card](131_instalacion_tarjetas_lovelace.md#tarjeta-mod-card)
- [Vertical stack in card](131_instalacion_tarjetas_lovelace.md#tarjeta-vertical-stack-in-card)

##### Configuración de tarjetas de la vista

- Dentro del directorio `db-pv/cards/host/` hay que crear los siguientes ficheros:
  - [Cabecera del bloque del equipo de HA](../config/lovelace/db-pv/cards/host/11_a_header_hassio.yaml)
  - [Bloque del equipo de HA](../config/lovelace/db-pv/cards/host/11_b_hassio.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/host/19_layout_break.yaml)
  - [Cabecera del bloque del SAI](../config/lovelace/db-pv/cards/host/21_a_header_ups.yaml)
  - [Bloque del SAI](../config/lovelace/db-pv/cards/host/21_b_ups.yaml)
  - [Cabecera del bloque de comunicaciones](../config/lovelace/db-pv/cards/host/22_a_header_network.yaml)
  - [Bloque de comunicaciones](../config/lovelace/db-pv/cards/host/22_b_network.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/host/29_layout_break.yaml)

### Vista: Network

![Vista de información de la red](images/22_network_view_scr.png)

- Dentro del directorio `db-pv/views/` hay que crear el fichero [22_network.yaml](../config/lovelace/db-pv/views/22_network.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Red
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista con información sobre la red.

    title: "Network"
    path: network
    icon: mdi:lan
    type: custom:vertical-layout
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/network
  ```

##### Tarjetas de terceros utilizadas en la vista

- [Layout card](131_instalacion_tarjetas_lovelace.md#tarjeta-layout-card)
- [Mod card](131_instalacion_tarjetas_lovelace.md#tarjeta-mod-card)

##### Configuración de tarjetas de la vista

- Dentro del directorio `db-pv/cards/network/` hay que crear los siguientes ficheros:
  - [Cabecera del bloque de interfaces del router](../config/lovelace/db-pv/cards/network/11_a_header_edgerouter.yaml)
  - [Bloque de interfaces del router](../config/lovelace/db-pv/cards/network/11_b_edgerouter.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/network/19_layout_break.yaml)

### Vista: Sensor

![Vista de información de los sensores](images/31_sensor_view_scr.png)

- Dentro del directorio `db-pv/views/` hay que crear el fichero [31_sensor.yaml](../config/lovelace/db-pv/views/31_sensor.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Sensor
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista de conjunto del estado de los elementos instalados.

    title: "Sensor"
    path: sensor
    icon: mdi:view-grid-outline
    type: custom:vertical-layout
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/sensor
  ```

##### Tarjetas de terceros utilizadas en la vista

- [Layout card](131_instalacion_tarjetas_lovelace.md#tarjeta-layout-card)
- [Mod card](131_instalacion_tarjetas_lovelace.md#tarjeta-mod-card)

##### Configuración de tarjetas de la vista

- Dentro del directorio `db-pv/cards/sensor/` hay que crear los siguientes ficheros:
  - [Cabecera del bloque del SAI](../config/lovelace/db-pv/cards/sensor/11_a_header_ups.yaml)
  - [Bloque del SAI](../config/lovelace/db-pv/cards/sensor/11_b_ups.yaml)
  - [Cabecera del bloque de enchufes](../config/lovelace/db-pv/cards/sensor/12_a_header_plug.yaml)
  - [Bloque de enchufes](../config/lovelace/db-pv/cards/sensor/12_b_plug.yaml)
  - [Cabecera del bloque de bombillas](../config/lovelace/db-pv/cards/sensor/13_a_header_lightbulb.yaml)
  - [Bloque de bombillas](../config/lovelace/db-pv/cards/sensor/13_b_lightbulb.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/sensor/19_layout_break.yaml)
  - [Cabecera del bloque de sensores de apertura/cierre](../config/lovelace/db-pv/cards/sensor/21_a_header_openclose.yaml)
  - [Bloque de sensores de apertura/cierre](../config/lovelace/db-pv/cards/sensor/21_b_openclose.yaml)
  - [Cabecera del bloque de sensores de inundación](../config/lovelace/db-pv/cards/sensor/22_a_header_flood.yaml)
  - [Bloque de sensores de inundación](../config/lovelace/db-pv/cards/sensor/22_b_flood.yaml)
  - [Cabecera del bloque de sensores de vibración](../config/lovelace/db-pv/cards/sensor/23_a_header_vibration.yaml)
  - [Bloque de sensores de vibración](../config/lovelace/db-pv/cards/sensor/23_b_vibration.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/sensor/29_layout_break.yaml)
  - [Cabecera del bloque de sensores de humedad y temperatura](../config/lovelace/db-pv/cards/sensor/31_a_header_weather.yaml)
  - [Bloque de sensoresde humedad y temperatura](../config/lovelace/db-pv/cards/sensor/31_b_weather.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/sensor/39_layout_break.yaml)

### Vista: Energy

![Vista de información de estado de baterías y consumo eléctrico](images/32_energy_view_scr.png)

- Dentro del directorio `db-pv/views/` hay que crear el fichero [32_energy.yaml](../config/lovelace/db-pv/views/32_energy.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Energía
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista de conjunto del estado de consumo eléctrico.

    title: "Energy"
    path: energy
    icon: mdi:flash
    type: custom:vertical-layout
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/energy
  ```

##### Tarjetas de terceros utilizadas en la vista

- [Battery state card](131_instalacion_tarjetas_lovelace.md#tarjeta-battery-state-card)
- [Layout card](131_instalacion_tarjetas_lovelace.md#tarjeta-layout-card)
- [Mod card](131_instalacion_tarjetas_lovelace.md#tarjeta-mod-card)
- [PVPC hourly pricing card](131_instalacion_tarjetas_lovelace.md#tarjeta-pvpc-hourly-pricing-card)

##### Configuración de tarjetas de la vista

- Dentro del directorio `db-pv/cards/energy/` hay que crear los siguientes ficheros:
  - [Cabecera del bloque de PVPC](../config/lovelace/db-pv/cards/energy/11_a_header_pvpc.yaml)
  - [Bloque de PVPC](../config/lovelace/db-pv/cards/energy/11_b_pvpc.yaml)
  - [Cabecera del bloque de consumo eléctrico](../config/lovelace/db-pv/cards/energy/12_a_header_energy_consumption.yaml)
  - [Bloque de consumo eléctrico](../config/lovelace/db-pv/cards/energy/12_b_energy_consumption.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/energy/19_layout_break.yaml)
  - [Cabecera del bloque de estado de la línea eléctrica](../config/lovelace/db-pv/cards/energy/21_a_header_status.yaml)
  - [Bloque de estado de la línea eléctrica](../config/lovelace/db-pv/cards/energy/21_b_status.yaml)
  - [Cabecera del bloque del SAI](../config/lovelace/db-pv/cards/energy/22_a_header_ups.yaml)
  - [Bloque del SAI](../config/lovelace/db-pv/cards/energy/22_b_ups.yaml)
  - [Cabecera del bloque de enchufes](../config/lovelace/db-pv/cards/energy/23_a_header_plug.yaml)
  - [Bloque de enchufes](../config/lovelace/db-pv/cards/energy/23_b_plug.yaml)
  - [Cabecera del bloque de interruptores](../config/lovelace/db-pv/cards/energy/24_a_header_switch.yaml)
  - [Bloque de interruptores](../config/lovelace/db-pv/cards/energy/24_b_switch.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/energy/29_layout_break.yaml)
  - [Cabecera del bloque de sensores de apertura/cierre](../config/lovelace/db-pv/cards/energy/31_a_header_openclose.yaml)
  - [Bloque de sensores de apertura cierre](../config/lovelace/db-pv/cards/energy/31_b_openclose.yaml)
  - [Cabecera del bloque de sensores de inundación](../config/lovelace/db-pv/cards/energy/32_a_header_flood.yaml)
  - [Bloque de sensores de inundación](../config/lovelace/db-pv/cards/energy/32_b_flood.yaml)
  - [Cabecera del bloque de sensores de humedad y temperatura](../config/lovelace/db-pv/cards/energy/33_a_header_weather.yaml)
  - [Bloque de sensores de humedad y temperatura](../config/lovelace/db-pv/cards/energy/33_b_weather.yaml)
  - [Cabecera del bloque de sensores de vibración](../config/lovelace/db-pv/cards/energy/34_a_header_vibration.yaml)
  - [Bloque de sensores de vibración](../config/lovelace/db-pv/cards/energy/34_b_vibration.yaml)
  - [Final de columna](../config/lovelace/db-pv/cards/energy/39_layout_break.yaml)

### Vista: Automation

- Dentro del directorio `db-pv/views/` hay que crear el fichero [41_automation.yaml](../config/lovelace/db-pv/views/41_automation.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Vista: Automatización
    ##

    # https://www.home-assistant.io/lovelace/dashboards-and-views/#views
    #
    # Vista de conjunto de las automatizaciones utilizadas habitualmente.

    title: "Automation"
    path: automation
    icon: mdi:robot-outline
    type: custom:vertical-layout
    visible:
      - user: !secret s_user

    badges: []
    cards: !include_dir_merge_list ../cards/automation
  ```

##### Configuración de tarjetas de la vista
