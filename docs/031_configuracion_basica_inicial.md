# Configuración básica inicial

- [Perfil del usuario](#perfil-del-usuario)
- [Configuración principal](#configuración-principal)
  - [Fichero 'configuration.yaml'](#fichero-configurationyaml)
  - [Fichero 'secrets.yaml'](#fichero-secretsyaml)
  - [Estructura del árbol de directorios](#estructura-del-árbol-de-directorios)
- [Configuración de las integraciones predeterminadas](#configuración-de-las-integraciones-predeterminadas)
  - [Integración: Automation](#integración-automation)
  - [Integración: Counter](#integración-counter)
  - [Integración: Frontend](#integración-frontend)
  - [Integración: Input boolean](#integración-input-boolean)
  - [Integración: Input button](#integración-input-button)
  - [Integración: Input datetime](#integración-input-datetime)
  - [Integración: Input number](#integración-input-number)
  - [Integración: Input select](#integración-input-select)
  - [Integración: Input text](#integración-input-text)
  - [Integración: Scene](#integración-scene)
  - [Integración: Script](#integración-script)
  - [Integración: Timer](#integración-timer)
  - [Integración: Zone](#integración-zone)
- [Configuración de otras integraciones necesarias](#configuración-de-otras-integraciones-necesarias)
  - [Integración: Binary sensor](#integración-binary-sensor)
  - [Integración: Command line](#integración-command-line)
  - [Integración: Group](#integración-group)
  - [Integración: Sensor](#integración-sensor)
  - [Integración: Template](#integración-template)
  - [Integración: TTS](#integración-tts)

## Perfil del usuario

Es importante que, en el perfil del usuario administrador, se encuentre activada la opción `Advanced mode` para poder acceder a determinadas opciones de configuración y control de _HomeAssistant_.

## Configuración principal

### Fichero 'configuration.yaml'

- Para facilitar la configuración y permitir una estructura más transparente, se han organizado de manera individual e independiente los diferentes elementos que componen la configuración.
- Se pueden cargar de diferentes maneras:
  - `!include ${NOMBRE_DE_FICHERO}`: incluye el contenido de `${NOMBRE_DE_FICHERO}`.
  - `!include_dir_list ${NOMBRE_DE_DIRECTORIO}`: incluye el contenido de los ficheros de `${NOMBRE_DE_DIRECTORIO}` como una lista. Cada fichero debe contener una única entrada.
  - `!include_dir_named ${NOMBRE_DE_DIRECTORIO}`: incluye el contenido de los ficheros de `${NOMBRE_DE_DIRECTORIO}` como un diccionario. Cada fichero debe contener una única entrada que se asociará con el nombre del mismo.
  - `!include_dir_merge_list ${NOMBRE_DE_DIRECTORIO}`: incluye el contenido de los ficheros de `${NOMBRE_DE_DIRECTORIO}` como una gran lista. Los ficheros deben contener una lista compuesta de una o más entradas.
  - `!include_dir_merge_named ${NOMBRE_DE_DIRECTORIO}`: incluye el contenido de los ficheros de `${NOMBRE_DE_DIRECTORIO}` como un gran diccionario. Los ficheros pueden contener una o más entradas que se asociarán con el nombre del mismo.

#### Contenido del fichero

- Contenido:
  - Los valores específicos correspondientes a la situación de la instalación.
  - Los proveedores de autenticación que se utilizarán.
  - Los mecanismos para cargar el resto de elementos que conforman la configuración de _HomeAssistant_.
- Editar el fichero de configuración principal, [configuration.yaml](../config/configuration.yaml), para dejarlo de la manera en que aparece a continuación.

  ```yaml
    ---
    ###
    ### Configuración principal
    ###

    # Basada en configuraciones de:
    # - Franck Nijhof
    #   https://github.com/frenck/home-assistant-config
    # - BeardedTinker
    #   https://github.com/BeardedTinker/Home-Assistant_Config

    ##
    ## Configuración del sistema
    ##

    # Carga de una serie de integraciones de manera predeterminada.
    # https://github.com/home-assistant/core/blob/dev/homeassistant/components/default_config/manifest.json
    default_config:

    # Configuración de las integraciones de manera individual.
    # La carga de algunas integraciones se hace de manera automática en el arranque,
    #   por lo que ya no se puede controlar desde el fichero de configuración.
    #automation:                # config/integrations/automation.yaml
    #binary_sensor:             # config/integrations/binary_sensor.yaml
    #command_line:              # config/integrations/command_line.yaml
    #counter:                   # config/integrations/counter.yaml
    #frontend:                  # config/integrations/frontend.yaml
    #group:                     # config/integrations/group.yaml
    #http:                      # config/integrations/http.yaml
    #input_boolean:             # config/integrations/input_boolean.yaml
    #input_button:              # config/integrations/input_button.yaml
    #input_datetime:            # config/integrations/input_datetime.yaml
    #input_number:              # config/integrations/input_number.yaml
    #input_select:              # config/integrations/input_select.yaml
    #input_text:                # config/integrations/input_text.yaml
    #lovelace:                  # config/integrations/lovelace.yaml
    #notify:                    # config/integrations/notify.yaml
    #recorder:                  # config/integrations/recorder.yaml
    #scene:                     # config/integrations/scene.yaml
    #script:                    # config/integrations/script.yaml
    #sensor:                    # config/integrations/sensor.yaml
    #telegram_bot:              # config/integrations/telegram_bot.yaml
    #template:                  # config/integrations/template.yaml
    #timer:                     # config/integrations/timer.yaml
    #tts:                       # config/integrations/tts.yaml
    #utility_meter:             # config/integrations/utility_meter.yaml
    #zone:                      # config/integrations/zone.yaml

    # Modificaciones a la configuración de HomeAssistant.
    homeassistant:
      name: "Home"
      elevation: !secret ha_elevation
      latitude: !secret ha_latitude
      longitude: !secret ha_longitude
      time_zone: !secret ha_timezone
      country: ES
      currency: EUR
      unit_system: metric

      # Acceso a HomeAssistant
      #external_url: ""
      internal_url: "https://hassio.penguin-village.net:12123"

      # Relación de los proveedores de autenticación
      auth_providers:
        - type: homeassistant

      # Carga de los diferentes módulos de configuración (packages)
      #  que conforman la configuración del sistema.
      packages: !include_dir_named integrations

      # Inclusión del directorio que contiene la configuración para
      #  la personalización de las entidades.
      customize: !include_dir_merge_named customizations/entities
  ```

### Fichero 'secrets.yaml'

#### Contenido del fichero

- Contenido:
  - Variables que contienen información sensible (datos del usuario, contraseñas, etc.) y que pueden ser referenciadas desde los ficheros de configuración mediante el parámetro `!secret`, de esa manera no es necesario incluir dichos valores en la propia configuración y facilita el compartir las configuraciones.
    > **Importante**<br/>
    > Es muy importante acordarse de no compartir con nadie el fichero secrets.yaml.
- Editar el fichero [secrets.yaml](../config/secrets.template.yaml), para dejarlo de la manera en que aparece a continuación y completar posteriormente los datos de las _variables_ que serán consultadas desde el fichero de configuración principal.

  ```yaml
    ---
    ###
    ### Almacén de datos privados (usuarios, contraseñas, etc.)
    ###

    # https://www.home-assistant.io/docs/configuration/secrets

    ##
    ## Datos de localización
    ##

    # Datos para la configuración de HomeAssistant
    ha_latitude: "${LATITUD_DE_HA}"
    ha_longitude: "${LONGITUD_DE_HA}"
    ha_elevation: "${ELEVACIÓN_DE_HA}"
    ha_timezone: "${HUSO_HORARIO_DE_HA}"

    ##
    ## Datos de usuarios
    ##

    s_user: "${ID_DEL_USUARIO}"
  ```

### Estructura del árbol de directorios

- Para recrear la base de la estructura de directorios que se va a utilizar en la presente configuración, es necesario aplicar los siguientes comandos desde dentro del directorio `config/` de _HomeAssistant_:

  ```shell
    config$ mkdir -p automations/{areas,system,tests}
    config$ mkdir backups
    config$ mkdir blueprints
    config$ mkdir -p customizations/entities/{areas,system,tests}
    config$ mkdir custom_components
    config$ mkdir -p entities/{binary_sensors,command_line,counters,groups}
    config$ mkdir -p entities/{input_booleans,input_datetimes,input_numbers,input_selects,input_texts}
    config$ mkdir -p entities/{notifiers,sensors,templates,timers,tts,zones}
    config$ mkdir gui
    config$ mkdir integrations
    config$ mkdir scenes
    config$ mkdir scripts
    config$ mkdir themes
    config$ mkdir -p www/community
  ```

## Configuración de las integraciones predeterminadas

En la configuración original de _HomeAssistant_ se incluye una llamada a la integración `default_config`, que es la encargada de activar el conjunto de integraciones predeterminadas.

### Integración: Automation

- Crear el fichero [automations.yaml](../config/automations.yaml) para almacenar las automatizaciones creadas desde el interfaz gráfico.
- Dentro del directorio `integrations/` hay que crear el fichero [automation.yaml](../config/integrations/automation.yaml), lo que permitirá crear automatizaciones desde el interfaz gráfico y mantener al mismo tiempo la posibilidad de seguir creándolas mediante ficheros _.yaml_.

### Integración: Counter

- Dentro del directorio `integrations/` hay que crear el fichero [counter.yaml](../config/integrations/counter.yaml).

### Integración: Frontend

- Dentro del directorio `integrations/` hay que crear el fichero [frontend.yaml](../config/integrations/frontend.yaml).

### Integración: Input boolean

- Dentro del directorio `integrations/` hay que crear el fichero [input_boolean.yaml](../config/integrations/input_boolean.yaml).

### Integración: Input button

- Dentro del directorio `integrations/` hay que crear el fichero [input_button.yaml](../config/integrations/input_button.yaml).

### Integración: Input datetime

- Dentro del directorio `integrations/` hay que crear el fichero [input_datetime.yaml](../config/integrations/input_datetime.yaml).

### Integración: Input number

- Dentro del directorio `integrations/` hay que crear el fichero [input_number.yaml](../config/integrations/input_number.yaml).

### Integración: Input select

- Dentro del directorio `integrations/` hay que crear el fichero [input_select.yaml](../config/integrations/input_select.yaml).

### Integración: Input text

- Dentro del directorio `integrations/` hay que crear el fichero [input_text.yaml](../config/integrations/input_text.yaml).

### Integración: Scene

- Crear el fichero [scenes.yaml](../config/scenes.yaml) para almacenar las escenas creadas desde el interfaz gráfico.
- Dentro del directorio `integrations/` hay que crear el fichero [scene.yaml](../config/integrations/scene.yaml), lo que permitirá crear escenas desde el interfaz gráfico y mantener al mismo tiempo la posibilidad de seguir creándolas mediante ficheros _.yaml_.

### Integración: Script

- Crear el fichero [scripts.yaml](../config/scripts.yaml) para almacenar los scripts creados desde el interfaz gráfico.
- Dentro del directorio `integrations/` hay que crear el fichero [script.yaml](../config/integrations/script.yaml), lo que permitirá crear scripts desde el interfaz gráfico y mantener al mismo tiempo la posibilidad de seguir creándolos mediante ficheros _.yaml_.

### Integración: Timer

- Dentro del directorio `integrations/` hay que crear el fichero [timer.yaml](../config/integrations/timer.yaml).

### Integración: Zone

- Dentro del directorio `integrations/` hay que crear el fichero [zone.yaml](../config/integrations/zone.yaml).

## Configuración de otras integraciones necesarias

A continuación se crean las configuraciones para otro conjunto de integraciones muy utilizadas, que están incluidas de serie en _HomeAssistant_, pero que no se configuran de manera predeterminada.

### Integración: Binary sensor

- Dentro del directorio `integrations/` hay que crear el fichero [binary_sensor.yaml](../config/integrations/binary_sensor.yaml).

### Integración: Command line

- Dentro del directorio `integrations/` hay que crear el fichero [command_line.yaml](../config/integrations/command_line.yaml).

### Integración: Group

- Dentro del directorio `integrations/` hay que crear el fichero [group.yaml](../config/integrations/group.yaml).

### Integración: Sensor

- Dentro del directorio `integrations/` hay que crear el fichero [sensor.yaml](../config/integrations/sensor.yaml).

### Integración: Template

- Dentro del directorio `integrations/` hay que crear el fichero [template.yaml](../config/integrations/template.yaml).

### Integración: TTS

- Dentro del directorio `integrations/` hay que crear el fichero [tts.yaml](../config/integrations/tts.yaml).
