# Integraciones de consumo energético

- [PVPC](#pvpc)
  - [Integración: ESIOS PVPC](#integración-esios-pvpc)
- [Shelly EM](#shelly-em)
  - [Integración: Shelly](#integración-shelly)
- [Control de la energía consumida](#control-de-la-energía-consumida)
  - [Integración: Energy](#integración-energy)
  - [Integración: Utility meter](#integración-utility-meter)
  - [Template: current-energy-tariff](#template-current-energy-tariff)
  - [Creación de automatizaciones](#creación-de-automatizaciones)

## PVPC

### Integración: ESIOS PVPC

Integrada en _Home Assistant_, por lo que se instala directamente desde el panel de integraciones.

- Valores para configurar la integración _ESIOS PVPC_
  - Tipo de tarifa contratada (PVPC, tarifa 2.0TD)
  - Potencia contratada en horario normal
  - Potencia contratada en horario valle

## Shelly EM

### Integración: Shelly

- Valores para configurar la integración _Shelly_:
  - `Host`: ${_IP_O_NOMBRE_DNS_DEL_SHELLY_EM_}
  - `Username`: ${_USUARIO_PARA_SHELLY_EM_}
  - `Password`: ${_CONTRASEÑA_PARA_SHELLY_EM_}
    > **Importante**<br/>
    > En caso de configurar el _Shelly EM_ utilizando _CoIoT_, hay que hacerlo como _unicast_, especificando la dirección _IP_ y el puerto _CoIoT_ de la instancia de _Home Assistant_.

#### Registro de la información de los sensores

- Para registrar en la base de datos información de algunos sensores, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.

  ```yaml
    ---
    ···
      include:
        ···
        entities:
          ···
          - binary_sensor.meter_sh_wf_001_overpowering
          - sensor.meter_sh_wf_001_uptime
          ···
  ```

## Control de la energía consumida

### Integración: Energy

- Dentro del directorio `integrations/` hay que crear el fichero [energy.yaml](../config/integrations/energy.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Gestión de energía
    ##

    # https://www.home-assistant.io/integrations/energy
    #
    # Integración habilitada de manera predeterminada en 'default_config:'.

    energy:
  ```

### Integración: Utility meter

- Dentro del directorio `integrations/` hay que crear el fichero [utility_meter.yaml](../config/integrations/utility_meter.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Medición de consumos
    ##

    # https://www.home-assistant.io/integrations/utility_meter

    # Establecemos diferentes contadores para las distintas franjas
    #   de consumo eléctrico y diferentes periodos.
    # La fuente de los datos es el sensor de consumo total del
    #   Shelly EM, que devuelve un valor en kWh.
    utility_meter:
      daily_energy:
        source: sensor.total_energy
        cycle: daily
        tariffs:
          - P1
          - P2
          - P3
  ```

### Template: current energy tariff

- Dentro del directorio `entities/templates/` hay que crear el fichero [current_energy_tariff_template.yaml](../config/integrations/current_energy_tariff_template.yaml) con el siguiente contenido.

  ```yaml
    ---
    ##
    ## Plantilla: tarifa eléctrica 2.0TD
    ##

    # https://www.home-assistant.io/docs/configuration/templating

    ## Cálculo de tarifa PVPC
    - sensor:
        - name: "current_energy_tariff"
          unique_id: current_energy_tariff
          state: >-
            {%  set _current_hour = ( now().hour | int )  %}

            {%  if ( is_state('sensor.is_national_holiday', 'off')
                     and  is_state('sensor.is_weekend', 'off') )
                   and  _current_hour >= 8  %}

              {%  if ( _current_hour >= 10  and  _current_hour < 14 )
                     or  ( _current_hour >= 18  and  _current_hour < 22 )  %}
                P1
              {%  else  %}
                P2
              {%  endif  %}

            {%  else  %}
              P3
            {%  endif  %}
  ```

#### Registro de la información de los sensores

- Para registrar en la base de datos información de algunos sensores, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.
  ```yaml
    ---
    ···
      include:
        ···
        entity_globs:
          ···
          - sensor.daily_energy_*
          ···
        entities:
          ···
          - select.daily_energy
          ···
  ```

### Creación de automatizaciones

#### Cambio de tarifa eléctrica automático

- Dentro del directorio `automations/system/` hay que crear el fichero [energy_tariff_change.yaml](../config/automations/system/energy_tariff_change.yaml) con su correspondiente contenido, para definir una automatización que se encargue de realizar los cambios de tarifa eléctrica en función de las franjas horarias del día.
