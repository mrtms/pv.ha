# Integración de un SAI empleando NUT

- [Eaton Ellipse ECO 1200VA](#eaton-ellipse-eco-1200va)
  - [Integración: Network UPS Tools (NUT)](#integración-network-ups-tools-nut)
  - [Registro de la información de los sensores](#registro-de-la-información-de-los-sensores)

## Eaton Ellipse ECO 1200VA

### Integración: Network UPS Tools (NUT)

- Desde el menú `Configuration` >> `Devices & services` >> `Integrations` es necesario añadir la integracion `Network UPS Tools (NUT)`. Para su configuración hay que usar los siguientes valores:
  - `Host`: ${_IP_O_NOMBRE_DNS_DEL_EQUIPO_DONDE_SE_HA_CREADO_EL_CONTENEDOR_DE_NUT-UPSD_} o `localhost` si está en el mismo equipo
  - `Port`: ${_PUERTO_DE_NUT-UPSD_}
  - `Username`: ${_USUARIO_PARA_NUT-UPSD_}
  - `Password`: ${_CONTRASEÑA_PARA_NUT-UPSD_} (la misma que se ha utilizado al crear el almacén de datos para el contenedor)

### Registro de la información de los sensores

- Para registrar en la base de datos información de algunos de los sensores de la integración, dentro del fichero [integrations/recorder.yaml](../config/integrations/recorder.yaml) hay que añadir los sensores.

  ```yaml
    ---
    ···
      ···
      include:
        ···
        entities:
          ···
          - sensor.eaton_load
          - sensor.eaton_status
          ···
  ```
